const app = require('/app');
const supertest = require('supertest')
const request = supertest(app);

const baseUrl = '/v1';


describe('GET /', () => {
	beforeAll(async (done) => {
		console.log('Before All Completed');
		done();
	});

	beforeEach(async (done) => {
		console.log('Before Each Completed');
		done();
	});

	afterEach(async (done) => {
		console.log('After Each Completed');
		done();
	});

	afterAll(async (done) => {
		console.log('After All Completed');
		done();
	});

	it('should return status 200 with message Welcome to the Application', async (done) => {
		const response = await request.get(baseUrl + '/');

		expect(response.status).toBe(200);
		expect(response.text).toBe('Welcome to the application.');
		done();
	})
})