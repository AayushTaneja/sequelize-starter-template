const mw = require('/utils/mw');
const vx = require('/utils/vx');
const k = require('/utils/k');

exports.getIndex = [
	mw.requestQueryValidator(null, null, null, null, null,
		vx.objectRequiredNotNull(
			{
				name: vx.stringOptionalNull(),
				age: vx.stringOptionalNull(),
				gender: vx.stringOptionalNull()
			},
			{
				name: ['age', 'gender']
			}
		)
	)
];

// Sample Data
//
// {
//     "string": "MALE",
//     "email": "aayushtaneja.12@in.aphelia.co",
//     "phoneNumber": "+16172817061",
//     "password": "BY37*&0und",
//     "url":"https://aphelia.co",
//     "ipv4": "255.255.123.123",
//     "ipv6": "2001:0db8:85a3:0000:0000:8a2e:0370:7334",
//     "int":3,
//     "number": 2.2,
//     "boolean": false,
//     "date": "2021-02-15",
//     "time": "12:35:00",
//     "dateTime": "2021-01-30T12:35:00Z",
//     "uuid": "fb729ac6-12ab-4631-93c3-5c000cbc6253",
//     "object": {
//         "nestedText": "apple"
//     },
//     "arrayPrimitive": ["apple", "ball"],
//     "arrayObject": [
//         {
//             "nestedText": "apple"
//         }
//     ]
// }
exports.postIndex = [
	mw.multipartFileSingle('sample-file', 2 * k.FILE_SIZES.MEGABYTE),
	mw.requestBodyValidator(
		vx.objectRequiredNull({
			string: vx.stringRequiredNotNull(3, 5, k.USER_GENDER_TYPES),
			// email: vx.stringFormatRequiredNotNull(k.VALIDATION_FORMATS.EMAIL),
			// phoneNumber: vx.stringFormatRequiredNotNull(k.VALIDATION_FORMATS.PHONE_NUMBER),
			// password: vx.stringFormatRequiredNotNull(k.VALIDATION_FORMATS.PASSWORD),
			// url: vx.stringFormatRequiredNotNull(k.VALIDATION_FORMATS.URL),
			// ipv4: vx.stringFormatRequiredNotNull(k.VALIDATION_FORMATS.IPV4),
			// ipv6: vx.stringFormatRequiredNotNull(k.VALIDATION_FORMATS.IPV6),
			// int: vx.integerRequiredNotNull(3, 4, [3, 6, 9], 3),
			// number: vx.numberRequiredNotNull( 1, 5, [1.1, 2.2, 3.3]),
			// boolean: vx.booleanRequiredNotNull(),
			// date: vx.dateOnlyRequiredNotNull( '2021-01-30', '2021-02-30'),
			// time: vx.timeOnlyRequiredNotNull( '10:00:00', '20:00:00'),
			// dateTime: vx.dateTimeRequiredNotNull( '2021-01-30T10:00:00.999Z', '2021-01-30T20:00:00Z'),
			// uuid: vx.uuidRequiredNotNull(),
			//
			// object:vx.objectRequiredNotNull(
			// 	{
			// 		nestedText: vx.stringRequiredNotNull( 3, 5, ['apple', 'ball'])
			// 	},
			// ),
			// arrayPrimitive: vx.arrayRequiredNotNull(
			// 	vx.stringRequiredNotNull(3, 5, ['apple', 'ball', 'gods', 'fatss']),
			// 	2,
			// 	3,
			// 	true
			// ),
			// arrayObject: vx.arrayRequiredNotNull(
			// 	vx.objectRequiredNotNull(
			// 		{
			// 			nestedText: vx.stringRequiredNotNull( 3, 5, ['apple', 'ball'])
			// 		},
			// 	),
			// ),

		})
	)
];