const express = require('express');
const router = express.Router();
const indexController = require('./index.controller');
const indexValidator = require('./index.validator');


router.get('/',
	indexValidator.getIndex,
	indexController.getIndex);

router.post('/',
	indexValidator.postIndex,
	indexController.getIndex);

module.exports = router;
