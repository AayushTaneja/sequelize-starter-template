const fn = require('/utils/fn');
const dynk = require('/utils/dynk');

exports.getIndex = fn.managedHandler(async (req, res, next) => {
	let dynamicConstants = await dynk.data;

	res.send({
		dynamicConstants: dynamicConstants
	});

	// todo
	//      0. Decide whether to have automated tests for waterfall projects? If yes, to go with developer written automated tests or with QA written automated tests
	//      1. document how to enable testing on node express app [migration code out of app, no asynchronous code in app.js]
	//         and how to write test cases. This will be needed on apps where automated testing is needed.
	//      2. update app to accommodate swagger api documentation
	//      3. Create separate app for just automated testing of the api, to be used by the tester. [With data seeders, All create and update routes first, then all get and delete routes]

	// todo enable test driven development



	// Test Cases CREATE
	// 1. it should throw validation error if required field not present
	// 2. it should be ok if optional field is present
	// 3. it should be ok if all fields are present
	// 4. it should throw validation error if other database constraints fail (such as unique, foreign key, check, etc)
	// 5. it should throw error if other service constraints fail
	// 6. it should throw validation error if other api validation constraints fail


	// Test Cases GET BY ID
	// 1. it should throw validation error if id not present
	// 2. it should be return 404 if item not found

	//todo complete test cases for PUT, PATCH, GET LIST, DELETE cases as well.

});

exports.postIndex = fn.managedHandler(async (req, res, next) => {
	return {
		data: req.body
	}
})