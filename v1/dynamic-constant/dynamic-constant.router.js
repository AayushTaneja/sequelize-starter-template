const express = require('express');
const router = express.Router();
const dynamicConstantController = require('./index.controller');
const dynamicConstantValidator = require('./index.validator');

router.post('/update/:key',
	dynamicConstantValidator.postUpdateDynamicConstant,
	dynamicConstantController.postUpdateDynamicConstant);

module.exports = router;
