const fn = require('/utils/fn');
const dynamicConstantService = require('./dynamic-constant.service');

exports.postUpdateDynamicConstant = fn.managedHandler(async (req, res, next) => {
	let updatedDynamicConstant = await dynamicConstantService.updateDynamicConstantByPKey(req.params.key, req.body);

	res.send({
		data: updatedDynamicConstant
	})
})