const mw = require('/utils/mw');
const vx = require('/utils/vx');
const fn = require('/utils/fn');
const dynk = require('/utils/dynk');

exports.postUpdateDynamicConstant = [
	mw.requestBodyValidator(
		vx.objectRequiredNotNull({
			value: vx.stringOptionalNotNull(),
			description: vx.stringOptionalNull()
		})
	),
	fn.managedHandler(async (req, res, next) => {
		if (fn.isPresent(req.body.value)) {
			dynk.validateDynamicConstantTextValue(req.params.key, req.body.value, true);
		}
		next();
	})
];