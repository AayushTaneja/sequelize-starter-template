const DynamicConstant = require('/models/dynamic-constant');
const {InternalServerError, NotFoundError, BadRequestError} = require('/utils/error');
const fn = require('/utils/fn');
const k = require('/utils/k');
const dynk = require('/utils/dynk');

exports.updateDynamicConstantByPKey = async (key, updateData) => {
	// throw new InternalServerError('Check for authorization. This certain type of users should be able to execute this operation.');

	if (fn.isPresent(updateData.value)) {
		dynk.validateDynamicConstantTextValue(key, updateData.value, false);
	}

	let dynamicConstant = await DynamicConstant.findOne({
		key: key
	});

	if (fn.isNotPresent(dynamicConstant)) {
		throw new NotFoundError('The requested dynamic constant does not exist.')
	}

	await dynamicConstant.update(updateData);

	return dynamicConstant;
}