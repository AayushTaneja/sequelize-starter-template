const indexRoutes = require('./index/index.router');
const dynamicConstantRoutes = require('./index/index.router');
const express = require('express');
const router = express.Router();

router.use(`/`, indexRoutes);
router.use('/dynamic-constant', dynamicConstantRoutes);


module.exports = router;
