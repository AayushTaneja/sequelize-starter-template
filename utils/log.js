const config = require('/utils/config');

if (config.NODE_ENV !== 'test'){
    const k = require('/utils/k');
    const axios = require('axios');
    const { createLogger, format, transports } = require('winston');
    const winston = require('winston');
    const { combine, timestamp, prettyPrint, printf } = format;
    const path = require('path');
    const dailyRotateFile = require('winston-daily-rotate-file');
    const LEVEL = Symbol.for('level');
    const myCustomLevels = {
        colors: {
            error: 'red',
            warn: 'yellow',
            info: 'blue',
            verbose: 'white',
            debug: 'green',
            silly: 'pink'
        }
    };
    winston.addColors(myCustomLevels.colors);

    const myFormat = printf(({level, message, label, timestamp }) => {
        return `\n=> ${level}    ${timestamp} \n${JSON.stringify(message, null, 2).replace(/\\n/g, '\n')+'\n'}`;
    });

    // const myFormat = printf(({level, message, label, timestamp }) => {
    // 	return `\n=> ${level}                            ${message.error? JSON.stringify(message.error) : message}\n   ${timestamp}\n`;
    // });

    const filterOnly = (level) => {
        return format(function (info) {
            if (info[LEVEL] === level) {
                return info;
            }
        })()
    }

    const remoteLogError = async (error) => {
        if (error === {}){
            return;
        }
        if (config.NODE_ENV !== k.NODE_ENVS.TEST && config.NODE_ENV !== k.NODE_ENVS.LOCAL){
            try{
                await axios.post('https://logging.aphelia.co/logs', {
                    projectTitle: config.PROJECT_TITLE,
                    writePassword: config.REMOTE_LOG_WRITE_PASSWORD,
                    error: {
                        message: error.reason,
                        statusCode: error.statusCode,
                        environment: error.environment,
                        endpoint: error.endpoint,
                        data: error.data,
                        client: error.client,
                        stack: error.stack,
                        user: error.user,
                        dbErrors: error.dbErrors
                    }
                });
            }
            catch (e) {
                console.log('Remote Logging Error: ', e);
            }
        }
    }

    let fileLogger;
    let consoleLogger;

    fileLogger = createLogger({
        format: combine(
            timestamp({format:'DD MMM YYYY, hh:mm A, ZZ'}),
            prettyPrint()
        ),
        transports: [
            new dailyRotateFile({
                filename: path.join(path.dirname(process.mainModule.filename), 'logs', 'error', 'error-%DATE%.log'),
                format: filterOnly('error'),
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
            }),
            new dailyRotateFile({
                filename: path.join(path.dirname(process.mainModule.filename), 'logs', 'info', 'info-%DATE%.log'),
                format: filterOnly('info'),
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
            }),
            new dailyRotateFile({
                filename: path.join(path.dirname(process.mainModule.filename), 'logs', 'warn', 'warn-%DATE%.log'),
                format: filterOnly('warn'),
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
            }),
        ],
        exceptionHandlers: [
            new dailyRotateFile({
                filename: path.join(path.dirname(process.mainModule.filename), 'logs', 'exception', 'exception-%DATE%.log'),
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
            }),
        ]
    });

    consoleLogger = createLogger({
        format: combine(
            format.colorize(),
            timestamp({format:'DD MMM YYYY, hh:mm A, ZZ'}),
            prettyPrint()
        ),
        transports: [
            new transports.Console({
                format: myFormat
            })
        ],
        exceptionHandlers: [
            new winston.transports.Console()
        ]
    });


    exports.error = (error) => {
        if (config.ENABLE_FILE_LOGGING) {
            fileLogger.error(error);
        }
        if (config.ENABLE_CONSOLE_LOGGING){
            consoleLogger.error(error);
        }
        remoteLogError(error);
    };

    exports.warn = (error) => {
        if (config.ENABLE_FILE_LOGGING) {
            fileLogger.warn(error);
        }
        if (config.ENABLE_CONSOLE_LOGGING){
            consoleLogger.warn(error);
        }
        remoteLogError(error);
    };

    exports.info = (message) => {
        if (config.ENABLE_FILE_LOGGING) {
            fileLogger.info(message);
        }
        if (config.ENABLE_CONSOLE_LOGGING){
            consoleLogger.info(message);
        }
    };
}
else{           //if NODE_ENV = test
    exports.error = (error) => {
        console.log('Error: ', error.message)
    };

    exports.warn = (error) => {
        console.log('Warn: ', error.message)
    };

    exports.info = (message) => {
        console.log('Info: ', message)
    };
}
