const k = require('/utils/k');
const config = {};
const listValidConfigDatatypes = [k.DATATYPES.STRING, k.DATATYPES.NUMBER, k.DATATYPES.BOOLEAN];

//================================ SETTING UP THE ENVIRONMENT VARIABLE SOURCE ==========================================

if (process.env.NODE_ENV === k.NODE_ENVS.TEST) {
    let envTest = require('dotenv').config({path: `./.env.test`});

    if (envTest.error) {
        throw new Error('.env.test file is required for running the test script.');
    }

    if (envTest.parsed.NODE_ENV !== k.NODE_ENVS.TEST){
        throw new Error('Invalid NODE_ENV in the .env.test file.');
    }

    // Extracting environment variables only from the .env.test file and ignoring any machine environment variables
    process.env = envTest.parsed;
}
else {
    require('dotenv').config();
}

config.NODE_ENV = process.env.NODE_ENV;

if (!Object.values(k.NODE_ENVS).includes(config.NODE_ENV)){
    throw new Error('Invalid NODE_ENV. Should be one of: [local, test, development, staging, production]');
}

//================================= CONFIGURATION VARIABLES ============================================================

// todo-upfront change this as per the application

config.SQL_DB = k.DATATYPES.STRING;
config.SQL_HOST = k.DATATYPES.STRING;
config.SQL_USER = k.DATATYPES.STRING;
config.SQL_PW = k.DATATYPES.STRING;
config.PORT = k.DATATYPES.NUMBER;
config.JWT_PRIVATE_KEY = k.DATATYPES.STRING;
config.JWT_PUBLIC_KEY = k.DATATYPES.STRING;
config.ENCRYPTION_KEY = k.DATATYPES.STRING;
config.ENABLE_FILE_LOGGING = k.DATATYPES.BOOLEAN;
config.ENABLE_CONSOLE_LOGGING = k.DATATYPES.BOOLEAN;
config.PROJECT_TITLE = k.DATATYPES.STRING;
config.REMOTE_LOG_WRITE_PASSWORD = k.DATATYPES.STRING;
config.REDIS_HOST = k.DATATYPES.STRING;
config.REDIS_PORT = k.DATATYPES.NUMBER;
config.REDIS_PASSWORD = k.DATATYPES.STRING;
config.S3_REGION = k.DATATYPES.STRING;
config.S3_BUCKET = k.DATATYPES.STRING;
config.S3_ACCESS_KEY_ID = k.DATATYPES.STRING;
config.S3_SECRET_ACCESS_KEY = k.DATATYPES.STRING;

const optionalConfigs = {
    [k.NODE_ENVS.LOCAL]: [
        'REMOTE_LOG_WRITE_PASSWORD',
        'PORT'
    ],
    [k.NODE_ENVS.TEST]: [
        'REMOTE_LOG_WRITE_PASSWORD',
        'PORT'
    ],
    [k.NODE_ENVS.DEVELOPMENT]: [
        'PORT'
    ],
    [k.NODE_ENVS.STAGING]: [
        'PORT'
    ],
    [k.NODE_ENVS.PRODUCTION]: [
        'PORT'
    ],
}

//================================ CONFIGURATION VALIDATION METHODS ====================================================

const validateConfigurationDatatype = () => {
    let listIncorrectDatatypeConfigs = [];

    Object.keys(config).forEach((key, index) => {
        if (key !== 'NODE_ENV' && !listValidConfigDatatypes.includes(config[key])){
            throw new Error('Check the datatype of the config description params for: ' + key);
        }

        let envValue = process.env[key];
        let numberEnvValue;

        if (envValue === null || envValue === undefined){
            return config[key] = undefined;
        }

        switch (config[key]) {
            case k.DATATYPES.BOOLEAN:
                if (envValue === 'true'){
                    config[key] = true;
                }
                else if (envValue === 'false'){
                    config[key] = false;
                }
                else {
                    listIncorrectDatatypeConfigs.push(key);
                }
                break;
            case k.DATATYPES.NUMBER:
                numberEnvValue = Number(envValue);
                if (isNaN(numberEnvValue)) {
                    return listIncorrectDatatypeConfigs.push(key);
                }
                config[key] = numberEnvValue;
                break;
            default:
                config[key] = envValue;
        }
    });

    if (listIncorrectDatatypeConfigs.length !== 0){
        let errorMessage = '\n\nThe following environment variables should be of the following datatypes: \n\n';
        listIncorrectDatatypeConfigs.forEach((key, index) => {
            errorMessage +=  key + ' ==> ' + config[key] + '\n';
        });

        throw new Error(errorMessage);
    }
};

const checkMissingConfigurations = () => {
    const listMissingConfigs = [];

    Object.keys(config).forEach((item, index) => {
        if ((config[item] === undefined || config[item] === '') && !optionalConfigs[config.NODE_ENV].includes(item)) {
            listMissingConfigs.push(item);
        }
    });

    if (listMissingConfigs.length !== 0) {
        throw new Error('Missing Environment Variables: ' + listMissingConfigs);
    }
};

const checkDatabaseNameAsPerEnvironment = () => {
    if (!config.SQL_DB.endsWith(`-${config.NODE_ENV}`)) {
        throw new Error(`The Database name for the environment "${config.NODE_ENV}" should end with "-${config.NODE_ENV}"`);
    }
}

const setUpConfiguration = () => {
    validateConfigurationDatatype();
    checkMissingConfigurations();
    checkDatabaseNameAsPerEnvironment();
};

//================================== EXECUTING CONFIGURATION SETUP =====================================================

setUpConfiguration();

Object.freeze(config);
module.exports = config;