const fn = {};
const k = require('/utils/k');
const jwt = require('jsonwebtoken');
const { v4: uuidV4 } = require('uuid');
const aws = require('aws-sdk');
const config = require('/utils/config');
const path = require('path');
const crypto = require('crypto');
const _ = require('lodash');
const {BadRequestError, UnauthorizedError, ForbiddenError, NotFoundError, InternalServerError, CustomError, INTERNAL_SERVER_ERROR_CODE} = require('/utils/error');
const log = require('/utils/log');
const Ajv = require('ajv');
const ajv = new Ajv({format:'full', allErrors: true, jsonPointers: true});
require('ajv-keywords')(ajv, ['allRequired','transform', 'formatMinimum', 'formatMaximum', 'uniqueItemProperties']);
require('ajv-errors')(ajv);

const s3 = new aws.S3({
    region: config.S3_REGION,
    accessKeyId: config.S3_ACCESS_KEY_ID,
    secretAccessKey: config.S3_SECRET_ACCESS_KEY,
});

fn.loggableError = async (error, req) => {
    let err = {};
    err.environment = config.NODE_ENV;
    err.statusCode = error.statusCode;
    err.reason = error.message;         // Using the key 'reason' because winston does not log objects with key 'message' properly
    err.stack = error.stack;
    err.isUncaughtException = error.isUncaughtException || false;
    err.isUnhandledRejection = error.isUnhandledRejection || false;

    if (req) {
        if (fn.isPresent(req.file)){
            delete req.file.buffer
        }

        if (fn.isPresent(req.files)){
            if (req.files instanceof Array){
                req.files.forEach((file, index) => {
                    delete file.buffer;
                })
            }
            else {
                Object.keys(req.files).forEach((key, index) => {
                    req.files[key].forEach((file, index) => {
                        delete file.buffer;
                    })
                });
            }
        }

        err.endpoint = {
            path: req._parsedUrl.pathname,
            method: req.method,
        };
        err.data = {
            body: req.body || {},
            params: req.params || {},
            query: req.query || {},
            file: req.file || null,
            files: req.files || null,
        };
        err.client = {
            contentType: req.header('Content-Type'),
            userAgent: req.header('user-agent'),
        };

        if (fn.isArrayOfDatatype(err.errors, 'object')){
            err.dbErrors = err.errors;
        }

        // todo-upfront customize for your application
		if (req.data.userId && req.data.sessionId){
            err.user = {
                userId: req.data.userId,
                userSessionId: req.data.sessionId
            };

            if (fn.isPresent(req.data.user)){
                err.userData = req.data.user;
            }
        }
        else {
            let token = req.headers['auth'];
            if (token) {
                return new Promise((resolve, reject) => {
                    jwt.verify(token, config.JWT_PUBLIC_KEY, k.JWT_OPTIONS, (err, token) => {
                        if (!err) {
                            // todo-upfront customize for your application
                            err.user = {
                                userId: token.userId,
                                sessionId: token.sessionId
                            };
                        }
                        resolve(err);
                    });
                });
            }
        }

    }

    return err;
};

fn.sanitizeAndReportError = async (error, req) => {
    if (!(error instanceof CustomError)) {
        if (error instanceof Error) {
            error.statusCode = INTERNAL_SERVER_ERROR_CODE;
        }
        else {
            error = new InternalServerError(error);
        }
    }

    let logError = await fn.loggableError(error, req);

    if (error instanceof InternalServerError || error.statusCode === INTERNAL_SERVER_ERROR_CODE) {
        log.error(logError);
    } else {
        log.warn(logError);
    }

    return error;
}

fn.generateAuthToken = (userId, sessionId) => {
    return "Bearer " + jwt.sign({userId, sessionId}, config.JWT_PRIVATE_KEY, k.JWT_OPTIONS);
};


fn.isInteger = (value) => {
    return Number.isInteger(value);
}

fn.isNumber = (value) => {
    return typeof value === 'number';
}

fn.isBoolean = (value) => {
    return typeof value === 'boolean';
}

fn.isString = (value) => {
    return typeof value === 'string';
}

// elementDatatype ==> number, string, object, boolean
fn.isArrayOfDatatype = (value, elementDatatype) => {
    if (value instanceof Array){
        let result = true;
        for(let i = 0; i < value.length; i++){
            if (typeof value[i] !== elementDatatype){
                result = false;
                break;
            }
        }
        return result;
    }
    else{
        return false
    }
}

fn.isObject = (value) => {
    return value instanceof Object;
}

fn.isPresent = (value) => {
    return (value !== null && value !== undefined);
}

fn.isNotPresent = (value) => {
    return (value === null || value === undefined);
}

fn.getFileSizeReadable = (fileSize) => {
    if (!fn.isNumber(fileSize) || fileSize < 0){
        throw new Error('File Size should be a positive number.')
    }

    if (fileSize < 1024){
        return fileSize.toFixed(1) + ' Bytes';
    }

    fileSize = fileSize/1024;

    if (fileSize < 1024){
        return fileSize.toFixed(1) + ' KiloBytes';
    }

    fileSize = fileSize/1024;

    if (fileSize < 1024){
        return fileSize.toFixed(1) + ' MegaBytes';
    }

    fileSize = fileSize/1024;

    if (fileSize < 1024){
        return fileSize.toFixed(1) + ' GigaBytes';
    }
}

//Pass the array of files provided by the multer req.files
fn.uploadMultipleToFileServer = async (files) => {
    if (!fn.isArrayOfDatatype(files, 'object')){
        throw new Error('Invalid list of files.');
    }

    let listFileUploadPromises = [];
    let listFileKeys = [];

    files.forEach((file, index) => {
        let fileKey = uuidV4() + path.extname(file.originalname);
        listFileKeys.push(fileKey);
        listFileUploadPromises.push(new Promise((resolve, reject) => {
            s3.upload({
                Bucket: config.S3_BUCKET,
                Key: fileKey,
                Body: file.buffer,
            }, (err, data) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        }));
    });

    await Promise.all(listFileUploadPromises);

    return listFileKeys;
}

// Make sure that the handler returns a promise i.e. async (req, res, next)
fn.managedHandler = (handler) => {
    return (req, res, next) => {
        handler(req, res, next).catch((e) => next(e));
    }
}

fn.isDateToday = (date) => {
    const today = new Date()
    return date.getDate() === today.getDate() &&
        date.getMonth() === today.getMonth() &&
        date.getFullYear() === today.getFullYear();
};

fn.extractUniqueConstraintKeysFromDbErrorMessage = (dbErrorMessage) => {
    let stringKeysList = dbErrorMessage.substring(
        dbErrorMessage.lastIndexOf('Key (') + 5,
        dbErrorMessage.lastIndexOf(')=(')
    );

    stringKeysList = stringKeysList.replace(/"/g, '');
    stringKeysList = stringKeysList.replace(/ /g, '');

    return stringKeysList.split(',');
}

fn.extractForeignKeyViolationFromDbErrorMessage = (dbErrorMessage) => {
    return dbErrorMessage.substring(
        dbErrorMessage.lastIndexOf('Key (') + 5,
        dbErrorMessage.lastIndexOf(')=(')
    );
}

//Encryption Functions

const ENCRYPTION_KEY = config.ENCRYPTION_KEY; // Must be 256 bits (32 characters)
const IV_LENGTH = 16; // For AES, this is always 16

fn.encrypt = (text) => {
    let iv = crypto.randomBytes(IV_LENGTH);
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + ':' + encrypted.toString('hex');
}

fn.decrypt = (text) => {
    let textParts = text.split(':');
    let iv = Buffer.from(textParts.shift(), 'hex');
    let encryptedText = Buffer.from(textParts.join(':'), 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
    let decrypted = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);

    return decrypted.toString();
}




//Catching Database Errors

fn.foreignKeyConstraint = (key, message) => {
    if (!fn.isString(key) || !fn.isString(message)){
        throw new InternalServerError('Invalid function parameters.')
    }
    return {key, message};
}

fn.uniqueConstraint = (listKeys, message) => {
    if (!fn.isArrayOfDatatype(listKeys, 'string') || !fn.isString(message)){
        throw new InternalServerError('Invalid function parameters.')
    }
    return {listKeys, message};
}

fn.checkConstraint = (constraint, message) => {
    if (!fn.isString(constraint) || !fn.isString(message)){
        throw new InternalServerError('Invalid function parameters.')
    }
    return {constraint, message};
}

fn.catchDatabaseErrors = (e, listUniqueConstraints, listForeignKeyConstraints, listCheckConstraints) => {
    if (fn.isNotPresent(e) || fn.isNotPresent(listUniqueConstraints) || fn.isNotPresent(listForeignKeyConstraints) || fn.isNotPresent(listCheckConstraints)) {
        throw new InternalServerError('Please provide all arguments for the function catchDatabaseErrors');
    }

    if (fn.isPresent(e.original)) {
        //UNIQUE CONSTRAINT ERRORS
        if (e.original.code === k.DB_ERROR_CODES.UNIQUE_CONSTRAINT_VIOLATED) {
            let uniqueFields = fn.extractUniqueConstraintKeysFromDbErrorMessage(e.original.detail);

            listUniqueConstraints.forEach((uniqueConstraint, index) => {
                if (_.isEqual(uniqueConstraint.listKeys, uniqueFields)) {
                    throw new ForbiddenError(uniqueConstraint.message);
                }
            });

            throw new Error('The following fields should be unique: ' + uniqueFields);
        }
        //FOREIGN KEY VIOLATION ERRORS
        else if (e.original.code === k.DB_ERROR_CODES.FOREIGN_KEY_VIOLATED) {
            let violatedForeignKey = fn.extractForeignKeyViolationFromDbErrorMessage(e.original.detail);

            listForeignKeyConstraints.forEach((foreignKeyConstraint, index) => {
                if (_.isEqual(foreignKeyConstraint.key, violatedForeignKey)) {
                    throw new BadRequestError(foreignKeyConstraint.message);
                }
            });

            throw new Error('Foreign Key Constraint Violated: ' + violatedForeignKey);
        }
        //CHECK CONSTRAINT VIOLATION
        else if(e.original.code === k.DB_ERROR_CODES.CHECK_CONSTRAINT_VIOLATED){
            let violatedCheckConstraint = e.original.constraint;

            listCheckConstraints.forEach((checkConstraint, index) => {
                if (_.isEqual(checkConstraint.constraint, violatedCheckConstraint)) {
                    throw new ForbiddenError(checkConstraint.message);
                }
            })

            throw new Error('Check Constraint Violated: ' + violatedCheckConstraint);
        }
        else {
            throw new InternalServerError(e);
        }
    }

    throw new InternalServerError(e);
}


fn.setModelServices = (Model, catchCreateHandler, catchUpdateHandler) => {
    Model.createAndHandleError = async (createData) => {
        let entity;
        try{
            entity = await Model.create(createData);
        }
        catch(e){
            catchCreateHandler(e);
        }
        return entity;
    }

    Model.bulkCreateAndHandleError = async (listCreateData, returning) => {
        let result;
        try{
            result = await Model.bulkCreate(listCreateData, {
                validate: true,
                returning,
            })
        }
        catch (e) {
            catchCreateHandler(e[0]);
        }

        return result;
    }

    Model.findOrCreateAndHandleError = async (where, defaults) => {
        let entity;
        try{
            entity = await Model.findOrCreate({
                where,
                defaults
            });
        }
        catch(e){
            catchCreateHandler(e);
        }
        return entity;
    }

    Model.updateAndHandleError = async(updateData, where, returning, limit, fields) => {
        let result;
        try {
            if (fn.isNotPresent(updateData) || fn.isNotPresent(where) || where === {}) {
                throw new InternalServerError('Invalid update parameters.')
            }

            result = await Model.update(updateData, {
                where,
                returning,
                limit,
                fields
            });
        }
        catch (e) {
            catchUpdateHandler(e);
        }

        return result;
    }

    Model.prototype.updateInstanceAndHandleError = async(updateData) => {
        try{
            await this.update(updateData);
        }
        catch(e){
            catchUpdateHandler(e);
        }
        return this;
    }

    Model.decrementAndHandleError = async (decrementFields, where, returning) => {
        let result;
        try{
            result = await Model.decrement(decrementFields, {
                where,
                returning
            });
        }
        catch (e) {
            catchUpdateHandler(e);
        }

        return result;
    }

    Model.prototype.decrementInstanceAndHandleError = async (decrementFields) => {
        try{
            await this.decrement(decrementFields);
        }
        catch (e) {
            catchUpdateHandler(e);
        }

        return this;
    }

    Model.incrementAndHandleError = async (incrementFields, where, returning) => {
        let result;
        try{
            result = await Model.increment(incrementFields, {
                where,
                returning
            });
        }
        catch (e) {
            catchUpdateHandler(e);
        }

        return result;
    }

    Model.prototype.incrementInstanceAndHandleError = async (incrementFields) => {
        try{
            await this.increment(incrementFields);
        }
        catch (e) {
            catchUpdateHandler(e);
        }

        return this;
    }
}

/*
* On encapsulating any object with this function, on accessing any non-existing or undefined-value property of the object will throw an error
*
* BEWARE: This function returns a new object and does not modify the original passed object. Remember to assign the returned value to the stated object
* For example:
* k = fn.disallowUndefinedProperties(k);
* */
fn.disallowUndefinedProperties = (obj) => {
    const handler = {
        get(target, property) {
            if (property in target || property === 'then' || property === 'toJSON') {
                return target[property];
            }

            throw new Error(`Property '${property}' is not defined`);
        }
    };

    return new Proxy(obj, handler);
}

/*
* This function trims all top-level string properties as well as nested string properties and strings within arrays or object arrays
* */
function trimmer(object) {
    return _.transform(object, (result, value, key) => {
        if (_.isObject(value)) {
            _.set(result, key, trimmer(value));
        } else {
            if (_.isString(value)) {
                _.set(result, key, _.trim(value));
            } else {
                _.set(result, key, value);
            }
        }
    });
}

fn.trimmer = trimmer;


/*
* This function allows us to query an array of objects which fulfill the filter criteria mentioned
* */

fn.findAllInObjectArray = (objectArray, filter) => {
    if (!fn.isArrayOfDatatype(objectArray, k.DATATYPES.OBJECT)) {
        throw new InternalServerError('The provided objectArray should be an array of objects.')
    }

    if (!fn.isObject(filter)) {
        throw new InternalServerError('The provided filter should be an object.')
    }

    let filterKeys = Object.keys(filter);
    return objectArray.filter(obj => {
        let countMatchedValues = 0;
        filterKeys.forEach((key, index) => {
            if (obj[key] === filter[key]) {
                countMatchedValues++;
            }
        });

        if (countMatchedValues === filterKeys.length) {
            return true;
        }
    });
};

fn.findOneInObjectArray = (array, filter) => {
    return fn.findAllInObjectArray(array, filter)[0];
}

fn.measurePromiseDuration = async (promise) => {
    let start = Date.now();
    try {
        await promise;
        return Date.now() - start;
    }
    catch (e) {
        return e;
    }
}

fn.validateDataUponSchema = (data, schema) => {
    const validator = ajv.compile(schema);

    if (!validator(data)){
        let errorMessage = 'Invalid ' + validator.errors[0].dataPath.slice(1) + ': ' + validator.errors[0].message;
        throw new Error(errorMessage);
    }
}

module.exports = fn;


