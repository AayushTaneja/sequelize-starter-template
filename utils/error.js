// HTTP Error Codes
const errorCodes = {
	OK: 200,

	BAD_REQUEST: 400,
	UNAUTHORIZED: 401,
	FORBIDDEN: 403,
	NOT_FOUND: 404,
	METHOD_NOT_ALLOWED: 405,

	INTERNAL_SERVER_ERROR: 500
};

class CustomError extends Error {
	statusCode = errorCodes.INTERNAL_SERVER_ERROR;

	constructor(message) {
		super(message);
	}
}

class BadRequestError extends CustomError {
	statusCode = errorCodes.BAD_REQUEST;

	constructor(message) {
		super(message);
	}
}

class UnauthorizedError extends CustomError {
	statusCode = errorCodes.UNAUTHORIZED;

	constructor(message) {
		super(message);
	}
}

class ForbiddenError extends CustomError {
	statusCode = errorCodes.FORBIDDEN;

	constructor(message) {
		super(message);
	}
}

class NotFoundError extends CustomError {
	statusCode = errorCodes.NOT_FOUND;

	constructor(message) {
		super(message);
	}
}

class MethodNotAllowedError extends CustomError {
	statusCode = errorCodes.METHOD_NOT_ALLOWED;

	constructor(message) {
		super(message);
	}
}


class InternalServerError extends CustomError {
	statusCode = errorCodes.INTERNAL_SERVER_ERROR;

	constructor(message) {
		super(message);
	}
}


module.exports = {
	CustomError,
	BadRequestError,
	UnauthorizedError,
	ForbiddenError,
	NotFoundError,
	MethodNotAllowedError,
	InternalServerError,
	INTERNAL_SERVER_ERROR_CODE: errorCodes.INTERNAL_SERVER_ERROR
}
