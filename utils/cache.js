const config = require('/utils/config');
const k = require('/utils/k');
const fn = require('/utils/fn');
const {InternalServerError} = require('/utils/error');
const DynamicConstant = require('/models/dynamic-constant');

/*
* Usage:
* 1. Modify the KEYS object to register cache data instances
* 2. In other files, use the get function to get the cache data instance
*
* Validation Criteria:
* 1. The KEYS object should have the exactly same key and nested key field.
* 2. The getterFunction for each key object should be of the type Function.
* */

// todo-upfront modify based on the application
const KEYS =  {
	DYNAMIC_CONSTANTS: {
		key: 'DYNAMIC_CONSTANTS',
		getterFunction: async () => {
			return await DynamicConstant.findAll();
		},
		cacheValidityDurationInSeconds: k.DURATIONS_IN_MILLISECONDS.HOUR		// Optional Field
	}
};

//------------------------------------ CACHE DATA INSTANCES VALIDATION -----------------------------------------------------------

Object.keys(KEYS).forEach((property, index) => {
	if (KEYS[property].key !== property) {
		throw new InternalServerError(`The KEYS object "${property}" should have the exactly same key and nested key field.`);
	}

	if (!(KEYS[property].getterFunction instanceof Function)){
		throw new InternalServerError(`The getterFunction for the "${property}" key object should be of the type Function.`);
	}

	if (fn.isPresent(KEYS[property].cacheValidityDurationInSeconds) && !fn.isInteger(KEYS[property].cacheValidityDurationInSeconds)) {
		throw new InternalServerError(`The provided cacheValidityDurationInSeconds for key "${property}" should be an integer.`);
	}
});

//------------------------------------ HELPER FUNCTIONS ---------------------------------------------------------------------------

const redis = require('redis');
const { promisify } = require('util');
let isConnected = false;
let isReady = false;

const defaultCacheValidityDurationInSeconds = k.DURATIONS_IN_SECONDS.HOUR;		// Cache Validity of 1 hour
const keyPrefix = `${config.PROJECT_TITLE}/${config.NODE_ENV}/`;

const client = redis.createClient({
	port: config.REDIS_PORT,
	host: config.REDIS_HOST,
	password: config.REDIS_PASSWORD,
	connect_timeout: k.DURATIONS_IN_MILLISECONDS.SECOND,
	socket_initial_delay: 2 * k.DURATIONS_IN_MILLISECONDS.HOUR,

	// Unlike SQL Database Instances, Redis Instances do not have named databases and instead work on indexes
	// Adding the prefix so that a common redis instance can be used for multiple projects and environments
	prefix: keyPrefix,
});

client.on('connect', () => {
	console.log('Redis Client Connected.');
	isConnected = true;
})

client.on('ready', () => {
	console.log('Redis Client Ready.');
	isReady = true;
})

client.on('error', async (err) => {
	console.log('Redis Client Error: ', err);
	isReady = false;
	// if (err.command !=='PING') {
	// 	await fn.sanitizeAndReportError(err);
	// }
})

client.on('end', () => {
	console.log('Redis Client Disconnected.');
	isConnected = false;
})

process.on('SIGINT', () => {
	client.quit();
	isConnected = isReady = false;
	process.exit(0);
})

// Pinging the Redis Server to keep the connection alive
// See https://gist.github.com/JonCole/925630df72be1351b21440625ff2671f#file-redis-bestpractices-node-js-md
setInterval(function(){
	client.ping();
}, k.DURATIONS_IN_MILLISECONDS.MINUTE);		// todo change duration



const getAsync = promisify(client.get).bind(client);
const setExAsync = promisify(client.setex).bind(client);
const pingAsync = promisify(client.ping).bind(client);

const getDataFromCacheOrOriginalSource = async (key, dataGetterFunction, customCacheValidityDurationInSeconds) => {
	if(!fn.isString(key)){
		throw new InternalServerError('The provided cache key should be a String.');
	}

	if (!(dataGetterFunction instanceof Function)) {
		throw new InternalServerError('The provided dataGetterPromise should be a Function.');
	}

	if (fn.isPresent(customCacheValidityDurationInSeconds) && !fn.isInteger(customCacheValidityDurationInSeconds)) {
		throw new InternalServerError('The provided customCacheValidityDurationInSeconds should be an integer.')
	}

	let data;
	let cacheValidityDurationInSeconds = customCacheValidityDurationInSeconds || defaultCacheValidityDurationInSeconds;

	if (isReady && isConnected) {
		try {
			data = JSON.parse(await getAsync(key));
		}
		catch (e) {
			await fn.sanitizeAndReportError('Redis getAsync Failed: ' + e);
		}

		if (data) {
			return data;
		}

		data = await dataGetterFunction();
		try {
			await setExAsync(key, cacheValidityDurationInSeconds, JSON.stringify(data));
		}
		catch (e) {
			await fn.sanitizeAndReportError('Redis setExAsync Failed: ' + e);
		}
	}
	else {
		data = await dataGetterFunction();
	}

	return data;
};

const get = async (keyObject) => {
	if (!fn.isString(keyObject.key)) {
		throw new InternalServerError('The provided key should be a string.');
	}

	if (!(keyObject.getterFunction instanceof Function)){
		throw new InternalServerError('The provided getterFunction should be a function.');
	}

	if (fn.isPresent(keyObject.cacheValidityDurationInSeconds) && !fn.isInteger(keyObject.cacheValidityDurationInSeconds)) {
		throw new InternalServerError('The provided cacheValidityDurationInSeconds should be an integer.')
	}

	return await getDataFromCacheOrOriginalSource(keyObject.key, keyObject.getterFunction, keyObject.cacheValidityDurationInSeconds);
}

const status = () => {
	return {
		isConnected,
		isReady
	}
}

const ping = async () => {
	if (isReady && isConnected) {
		return pingAsync;
	}
	else {
		throw new Error('Redis not ready or not connected.')
	}
}

module.exports = {
	KEYS,
	get,
	ping,
	status
};