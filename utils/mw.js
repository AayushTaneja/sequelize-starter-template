const helmet = require('helmet');
const {BadRequestError, UnauthorizedError, ForbiddenError, NotFoundError, MethodNotAllowedError, InternalServerError, INTERNAL_SERVER_ERROR_CODE} = require('/utils/error');
const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const path = require('path');
const rfs = require('rotating-file-stream');
const fn = require('/utils/fn');
const k = require('/utils/k');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const cors = require('cors');
const config = require('/utils/config');
const Ajv = require('ajv');
const ajv = new Ajv({format:'full', allErrors: true, jsonPointers: true});
require('ajv-keywords')(ajv, ['allRequired','transform', 'formatMinimum', 'formatMaximum', 'uniqueItemProperties']);
require('ajv-errors')(ajv);
const User = require('/models/user');
const sql = require('/database/sql');
const cache = require('/utils/cache');

ajv.addKeyword('regexp', {
    validate: function regexp(schema, data) {
        if (schema.test(data)){
            return true
        }

        regexp.errors = [];
        regexp.errors.push({
            keyword: 'regexp',
            message: 'is not following the valid format.'
        });
        return false;
    },
    errors: true
});

ajv.addFormat(k.VALIDATION_FORMATS.PHONE_NUMBER, k.REGEXPS.PHONE_NUMBER);
ajv.addFormat(k.VALIDATION_FORMATS.PASSWORD, k.REGEXPS.PASSWORD);



//===============================================BEGIN BEFORE ROUTES MIDDLEWARE==================================================


let requestFileLogger;
let requestConsoleLogger;

if (config.ENABLE_FILE_LOGGING && config.NODE_ENV !== k.NODE_ENVS.TEST) {
    const logDirectory = path.join(
        path.dirname(process.mainModule.filename),
        'logs',
        'requests'
    );
    fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
    let accessLogStream = rfs.createStream('access.log', {
        interval: '1d',
        path: logDirectory
    });

    requestFileLogger = morgan(
        '[:date[clf]] :method :url       Status: :status, Response Time: :response-time ms, Content Length: :res[content-length]',
        { stream: accessLogStream }
    )
}

if(config.ENABLE_CONSOLE_LOGGING && config.NODE_ENV !== k.NODE_ENVS.TEST) {
    requestConsoleLogger = morgan('[:date[clf]] :method :url       Status: :status, Response Time: :response-time ms, Content Length: :res[content-length]')
}

const setupDataObjectInRequest = (req, res, next) => {
    req.data = {};
    next();
}

const checkProjectTitleAndEnvironment = fn.managedHandler(async (req, res, next) => {
    let projectTitle = req.headers['project-title'];
    let environment = req.headers['environment'];

    if (fn.isNotPresent(projectTitle)){
        throw new BadRequestError('Please set the header: Project-Title');
    }

    if (fn.isNotPresent(environment)){
        throw new BadRequestError('Please set the header: Environment');
    }

    if (projectTitle !== config.PROJECT_TITLE){
        throw new BadRequestError('Incorrect value for header: Project-Title');
    }

    if (environment !== config.NODE_ENV){
        throw new BadRequestError('Incorrect value for header: Environment');

    }

    next();
});

const limitRequestSize = fn.managedHandler(async (req, res, next) => {
        if (req.is('multipart/form-data')) {
            if (Number(req.headers['content-length']) > k.REQUEST_TOTAL_SIZE_MAX){
                throw new BadRequestError('The request payload size should be maximum ' + fn.getFileSizeReadable(k.REQUEST_TOTAL_SIZE_MAX));
            }
        }
        else {
            if (Number(req.headers['content-length']) > k.REQUEST_DATA_SIZE_MAX){
                throw new BadRequestError('The request payload size should be maximum ' + fn.getFileSizeReadable(k.REQUEST_DATA_SIZE_MAX));
            }
        }
        next();
});

/*
* This middleware trims the request body and query string properties as well as nested string properties and strings within arrays or object arrays
* */
const requestTrimmer = fn.managedHandler(async (req, res, next) => {
    if (req.body) {
        req.body = fn.trimmer(req.body);
    }
    if (req.query) {
        req.query = fn.trimmer(req.query);
    }
    next();
});

function beforeRoutes(app) {
    app.use(setupDataObjectInRequest);
    // app.use(checkProjectTitleAndEnvironment);
    app.use(cors());
    app.use(helmet());
    app.use(express.json());
    app.use(requestTrimmer);
    app.use(limitRequestSize);
    if (requestFileLogger) {
        app.use(requestFileLogger);
    }
    if (requestConsoleLogger) {
        app.use(requestConsoleLogger);
    }
}


//-----------------------------------------------END BEFORE ROUTES MIDDLEWARE----------------------------------------------------







//===============================================BEGIN AFTER ROUTES MIDDLEWARE===================================================

async function errorHandler (error, req, res, next) {
    error = await fn.sanitizeAndReportError(error, req);

    error.message = (error instanceof InternalServerError || error.statusCode === INTERNAL_SERVER_ERROR_CODE)
            ? 'Internal server error. Please try again later.'
            : error.message;

    res.status(error.statusCode).send({ error: error.message });
}

function endpointNotFoundHandler(req, res, next) {
    next(new MethodNotAllowedError('Endpoint Not Found'));
}


function afterRoutes(app) {
    app.use('/', endpointNotFoundHandler);
    app.use(errorHandler);
}

//------------------------------------------------END AFTER ROUTES MIDDLEWARE----------------------------------------------------







//==============================================BEGIN FILE HANDLING MIDDLEWARE==================================================


const extractDataFromMultipart = (req) => {
    let data = {};

    if (fn.isPresent(req.body.data)){
        if (typeof req.body.data !== k.DATATYPES.STRING){
            throw new BadRequestError('Data should be a non empty json string.');
        }

        try{
            while(typeof req.body.data === k.DATATYPES.STRING){
                req.body.data = JSON.parse(req.body.data);
            }
            data = req.body.data;
        }
        catch(e) {
            throw new BadRequestError('Data cannot be parsed.');
        }
    }

    return req.body = data;
};

const multipartParser = multer({
    storage: multer.memoryStorage(),
    onError : function(err, next) {
        next(err);
    },
});

// fileArrayName, maxCount and sizeMaxEach are required
// minCount = 1 by default, needs to be explicitly set to 0
// files accessible in controller via req.files
// allowedFormats can either be a string array or null, but not an empty array

const multipartFileArray = (fileArrayName, maxCount, sizeMaxEach, minCount, allowedFormats) => {
    if (!fn.isString(fileArrayName)) {
        throw new Error('File Array Name is required');
    }
    if (!fn.isInteger(maxCount) || maxCount <= 0){
        throw new Error('Please set a positive integer value for maxCount.');
    }
    if (!fn.isNumber(sizeMaxEach) || sizeMaxEach <= 0){
        throw new Error('Please set a positive number value for sizeMaxEach.');
    }
    if ((sizeMaxEach * maxCount) > k.REQUEST_FILE_SIZE_MAX) {
        throw new Error('The allowed collective file sizes is set as greater than the maximum allowed request file size. Either increase the maximum allowed request file size or else decrease the allowed collective file size.')
    }
    if (fn.isPresent(minCount) && !fn.isInteger(minCount)){
        throw new Error('Invalid value for minCount.');
    }
    if (fn.isInteger(minCount) && minCount < 0){
        throw new Error('minCount cannot be less than 0.');
    }
    if (fn.isNotPresent(minCount)){
        minCount = 1;           //minCount default value if not explicitly set to 0
    }
    if (fn.isPresent(allowedFormats) && !fn.isArrayOfDatatype(allowedFormats, k.DATATYPES.STRING)){
        throw new Error('Invalid value for allowedFormats.');
    }
    if (fn.isArrayOfDatatype(allowedFormats) && allowedFormats.length === 0){
        throw new Error('allowedFormats cannot be an empty array.');
    }
    // if (!fn.isNumber(sizeMaxTotal) && fn.isNotNullNotUndefined(sizeMaxTotal)){
    //     throw new Error('Invalid value for sizeMaxTotal.');
    // }
    // if (fn.isInteger(sizeMaxTotal) && sizeMaxTotal <= 0){
    //     throw new Error('sizeMaxTotal cannot be less than or equal to 0.');
    // }

    return (req, res, next) => {
        multipartParser.array(fileArrayName, maxCount)(req, res, err => {
            try{
                extractDataFromMultipart(req, next);

                if (err) {
                    throw new BadRequestError('Please upload upto ' + maxCount + ' ' + fileArrayName + ' files.');
                }

                if (minCount || (req.files && req.files.length > 0)){
                    if (minCount && (!req.files || req.files.length < minCount)) {
                        throw new BadRequestError(`Please upload at least ${minCount} ${fileArrayName} files`);
                    }

                    if (allowedFormats) {
                        req.files.every((item, index) => {
                            if (allowedFormats.indexOf(item.mimetype) < 0) {
                                throw new BadRequestError('Please ensure that all files are of formats: ' + allowedFormats);
                            }
                        });
                    }

                    req.files.every((item, index) => {
                        if (item.size > sizeMaxEach) {
                            throw new BadRequestError('Please ensure that each file has size less than: ' + fn.getFileSizeReadable(sizeMaxEach));
                        }
                    });

                    // if (sizeMaxTotal) {
                    //     let sizeRequestFilesTotal = 0;
                    //     req.files.forEach((item, index) => {
                    //         sizeRequestFilesTotal += item.size;
                    //     });
                    //     if  (sizeRequestFilesTotal > sizeMaxTotal) {
                    //         throw new BadRequestError('Please ensure that total file size be less than: ' + fn.getFileSizeReadable(sizeMaxTotal)));
                    //         return false;
                    //     }
                    // }
                }

                next();
            }
            catch (e) {
                next(e);
            }
        });
    }
};

// fileName and sizeMax are required
// isRequired = true by default, needs to be explicitly set to false
// file is accessible in controller via req.file
// allowedFormats can either be a string array or null, but not an empty array

const multipartFileSingle = (fileName, sizeMax, isRequired, allowedFormats) => {
    if (!fn.isString(fileName)) {
        throw new Error('File Name is required');
    }
    if (!fn.isNumber(sizeMax) || sizeMax <= 0){
        throw new Error('Please provide a positive number value for sizeMax.');
    }
    if (sizeMax > k.REQUEST_FILE_SIZE_MAX){
        throw new Error('The allowed file size is greater than the maximum allowed request file size. Either increase the maximum allowed request file size or decrease the allowed file size.')
    }
    if (!fn.isBoolean(isRequired) && fn.isPresent(isRequired)){
        throw new Error('Invalid value for isRequired.');
    }
    if (fn.isNotPresent(isRequired)){        //isRequired default is true unless set to false
        isRequired = true;
    }
    if (!fn.isArrayOfDatatype(allowedFormats, k.DATATYPES.STRING) && fn.isPresent(allowedFormats)){
        throw new Error('Invalid value for allowedFormats.');
    }
    if (fn.isArrayOfDatatype(allowedFormats) && allowedFormats.length === 0){
        throw new Error('allowedFormats cannot be an empty array.');
    }


    return (req, res, next) => {
        multipartParser.single(fileName)(req, res, err => {
            try{
                extractDataFromMultipart(req, next);

                if (err) {
                    throw new BadRequestError('Please upload single file only with the key: ' + fileName);
                }

                if (isRequired || req.file) {

                    if(!req.file) {
                        throw new BadRequestError(`Please upload ${fileName} file.`);
                    }

                    if (allowedFormats) {
                        if (allowedFormats.indexOf(req.file.mimetype) < 0) {
                            throw new BadRequestError('Please ensure that file is of formats: ' + allowedFormats);
                        }
                    }

                    if (req.file.size > sizeMax) {
                        throw new BadRequestError('Please ensure that file has size less than: ' + fn.getFileSizeReadable(sizeMax));
                    }
                }

                next();
            }
            catch (e) {
                next(e);
            }
        });
    }
};

// filesMapArray Format:
// [
//      {
//          name: 'fieldOne',                   // string  | required
//          maxCount: 1,                        // integer | required
//          sizeMaxEach: k.FILE_SIZES.MEGABYTE,  // number  | required
//          minCount: 0,                        // integer (non-negative) | optional | default = 1
//          allowedFormats: ['application/pdf'] // array(string) (must not be an empty array) | optional
//      }
// ]
//
// files accessible in controller via req.files.fieldOne[0]

const multipartFilesCustom = (filesMapArray) => {
    if (!fn.isArrayOfDatatype(filesMapArray, k.DATATYPES.OBJECT)) {
        throw new Error('filesMapArray should be an array of objects.');
    }

    let filesCountMin;
    let totalAllowedFileSizeMax = 0;

    for (let i = 0; i < filesMapArray.length ; i++){
        if (!fn.isString(filesMapArray[i].name)) {
            throw new Error('File Name is required at index: ' + i);
        }
        if (!fn.isInteger(filesMapArray[i].maxCount) || filesMapArray[i].maxCount <= 0){
            throw new Error('Please set a positive integer value for maxCount at index: ' + i);
        }
        if (!fn.isNumber(filesMapArray[i].sizeMaxEach) || filesMapArray[i].sizeMaxEach <= 0) {
            throw new Error('Please provide positive number value for sizeMaxEach at index: ' + i);
        }
        if (fn.isPresent(filesMapArray[i].minCount) && !fn.isInteger(filesMapArray[i].minCount)){
            throw new Error('Invalid value for minCount at index: ' + i);
        }
        if (fn.isInteger(filesMapArray[i].minCount) && filesMapArray[i].minCount < 0){
            throw new Error('minCount cannot be less than 0 at index: ' + i);
        }
        if (fn.isNotPresent(filesMapArray[i].minCount)){
            filesMapArray[i].minCount = 1;           //minCount default value if not explicitly set to 0
        }
        if (fn.isPresent(filesMapArray[i].allowedFormats) && !fn.isArrayOfDatatype(filesMapArray[i].allowedFormats, k.DATATYPES.STRING)){
            throw new Error('Invalid value for allowedFormats at index: ' + i);
        }
        if (fn.isArrayOfDatatype(filesMapArray[i].allowedFormats) && filesMapArray[i].allowedFormats.length === 0){
            throw new Error('allowedFormats cannot be an empty array at index: ' + i);
        }

        filesCountMin  = filesCountMin || filesMapArray[i].minCount;
        totalAllowedFileSizeMax += (filesMapArray[i].maxCount * filesMapArray[i].sizeMaxEach);
    }

    if (totalAllowedFileSizeMax > k.REQUEST_FILE_SIZE_MAX){
        throw new Error('The total allowed collective file size is greater than the maximum allowed request file size. Either increase the maximum allowed request file size or decrease the total allowed collective file size.')
    }

    return (req, res, next) => {
        multipartParser.fields(filesMapArray)(req, res, err => {
            try {
                extractDataFromMultipart(req, next);

                if (err) {
                    throw new BadRequestError('Please verify the file keys and count.');
                }

                // For the case that stated minCount for all files is Zero and the request body has no files, this block of code does not run.
                if (filesCountMin || (req.files)){
                    if (!req.files) {
                        throw new BadRequestError('Please upload the required files.');
                    }

                    filesMapArray.every((item, index) => {

                        if (!req.files[item.name]) {
                            req.files[item.name] = [];
                        }

                        if (req.files[item.name].length < item.minCount) {
                            throw new BadRequestError('Please ensure that ' + item.name + ' has at least ' + item.minCount + ' files.');
                        }

                        if (item.allowedFormats) {
                            req.files[item.name].every((file,j) => {
                                if (item.allowedFormats.indexOf(file.mimetype) < 0) {
                                    throw new BadRequestError('Please ensure that ' + item.name + ' is/are of formats: ' + item.allowedFormats);
                                }
                            });
                        }

                        if (item.sizeMaxEach) {
                            req.files[item.name].every((file,j) => {
                                if (file.size > item.sizeMaxEach) {
                                    throw new BadRequestError('Please ensure that ' + item.name + ' has size each less than ' + fn.getFileSizeReadable(item.sizeMaxEach));
                                }
                            });
                        }
                    });
                }

                next();
            }
            catch (e) {
                next(e);
            }
        });
    }
};


//-----------------------------------------------END FILE HANDLING MIDDLEWARE----------------------------------------------------





//===============================================BEGIN REQUEST VALIDATION MIDDLEWARE=============================================

const checkIfAllFieldsExistsInSchema = (listFields, schemaProperties) => {
    if (fn.isPresent(listFields)){
        if (!fn.isArrayOfDatatype(listFields, k.DATATYPES.STRING) || listFields.length === 0){
            throw new Error('The list of fields should be either a non empty string array or null.');
        }
        listFields.forEach((field, index) => {
            if (!schemaProperties.includes(field)){
                throw new Error('The field to be sanitized does not exist in the schema: ' + field);
            }
        });
    }
}

const requestQueryBooleanSanitizer = (listBooleanFields, req) => {

    if (listBooleanFields === null){
        return;
    }

    listBooleanFields.forEach((field, index) => {
        if (fn.isPresent(req.query[field])){
            if (req.query[field] === 'true' || req.query[field] === '1'){
                req.query[field] = true;
            }
            else if (req.query[field] === 'false' || req.query[field] === '0'){
                req.query[field] = false;
            }
            else {
                throw new BadRequestError('Invalid: ' + field + ': should be a boolean.');
            }
        }
    });

};

const requestQueryNumberSanitizer = (listNumberFields, req) => {

    if (listNumberFields === null){
        return;
    }

    listNumberFields.forEach((field, index) => {
        if (fn.isPresent(req.query[field])){
            let fieldNumber = req.query[field] * 1;
            if (isNaN(fieldNumber)){
                throw new BadRequestError('Invalid: ' + field + ': should be a number.');
            }
            req.query[field] = fieldNumber;
        }
    });

};

const requestQueryBooleanArraySanitizer = (listBooleanArrayFields, req) => {

    if (listBooleanArrayFields === null){
        return;
    }

    listBooleanArrayFields.forEach((field, fieldIndex) => {
        if (fn.isPresent(req.query[field])){
            if (typeof req.query[field] === k.DATATYPES.STRING){
                if (req.query[field] === 'true' || req.query[field] === '1'){
                    req.query[field] = [true];
                }
                else if (req.query[field] === 'false' || req.query[field] === '0'){
                    req.query[field] = [false];
                }
                else {
                    throw new BadRequestError('Invalid: ' + field + ': should be a boolean array.');
                }
            }
            else{
                req.query[field].forEach((value, valueIndex) => {
                    if (value === 'true' || value === '1'){
                        req.query[field][valueIndex] = true;
                    }
                    else if (value === 'false' || value === '0'){
                        req.query[field][valueIndex] = false;
                    }
                    else {
                        throw new BadRequestError('Invalid: ' + field + ': should be a boolean array.');
                    }
                })
            }
        }
    });

};

const requestQueryNumberArraySanitizer = (listNumberArrayFields, req) => {

    if (listNumberArrayFields === null){
        return;
    }

    let fieldNumber;
    listNumberArrayFields.forEach((field, fieldIndex) => {
        if (fn.isPresent(req.query[field])){
            if (typeof req.query[field] === k.DATATYPES.STRING){
                fieldNumber = req.query[field] * 1;
                if (isNaN(fieldNumber)){
                    throw new BadRequestError('Invalid: ' + field + ': should be a number array.');
                }
                req.query[field] = [fieldNumber];
            }
            else{
                req.query[field].forEach((value, valueIndex) => {
                    fieldNumber = value * 1;
                    if (isNaN(fieldNumber)){
                        throw new BadRequestError('Invalid: ' + field + ': should be a number array.');
                    }
                    req.query[field][valueIndex] = fieldNumber;
                })
            }
        }
    });

};

const requestQueryStringArraySanitizer = (listStringArrayFields, req) => {

    if (listStringArrayFields === null){
        return;
    }

    listStringArrayFields.forEach((field, fieldIndex) => {
        if (fn.isPresent(req.query[field])){
            if (typeof req.query[field] === k.DATATYPES.STRING){
                req.query[field] = [req.query[field]];
            }
        }
    });

};

// either provide array or null, but not empty arrays
const requestQueryValidator = (listNumberFields, listBooleanFields, listStringArrayFields, listNumberArrayFields, listBooleanArrayFields, schema) => {
    if (!fn.isObject(schema)) {
        throw new Error('Please provide valid schema.')
    }

    let schemaProperties = Object.keys(schema.properties);

    checkIfAllFieldsExistsInSchema(listNumberFields, schemaProperties);
    checkIfAllFieldsExistsInSchema(listBooleanFields, schemaProperties);
    checkIfAllFieldsExistsInSchema(listStringArrayFields, schemaProperties);
    checkIfAllFieldsExistsInSchema(listNumberArrayFields, schemaProperties);
    checkIfAllFieldsExistsInSchema(listBooleanArrayFields, schemaProperties);

    return fn.managedHandler(async (req, res, next) => {
        requestQueryNumberSanitizer(listNumberFields, req);
        requestQueryBooleanSanitizer(listBooleanFields, req);
        requestQueryStringArraySanitizer(listStringArrayFields, req);
        requestQueryBooleanArraySanitizer(listBooleanArrayFields, req);
        requestQueryNumberArraySanitizer(listNumberArrayFields, req);

        let validator = ajv.compile(schema);
        if (!validator(req.query)){
            let errorMessage = 'Invalid ' + validator.errors[0].dataPath.slice(1) + ': ' + validator.errors[0].message;
            throw new BadRequestError(errorMessage);
        }

        next();
    })
};

const requestBodyValidator = (schema) => {
    if (!fn.isObject(schema)) {
        throw new Error('Please provide the schema.')
    }
    return fn.managedHandler(async (req, res, next) => {
        let validator = ajv.compile(schema);
        if (!validator(req.body)){
            let errorMessage = 'Invalid ' + validator.errors[0].dataPath.slice(1) + ': ' + validator.errors[0].message;
            throw new BadRequestError(errorMessage);
        }

        next();
    })
};


//------------------------------------------------END REQUEST VALIDATION MIDDLEWARE----------------------------------------------





//===================================BEGIN PROJECT CUSTOM MIDDLEWARE=============================================================


const isAuth = fn.managedHandler(async (req, res, next) => {
    let authorizationHeader = req.headers['authorization'];

    let token = authorizationHeader ? authorizationHeader.split('Bearer ')[1] : null;

    if (token) {
        jwt.verify(token, config.JWT_PUBLIC_KEY, k.JWT_OPTIONS, (err, token) => {
            if (err) {
                throw new UnauthorizedError('Session Expired. Please log in again.');
            } else {
                req.data.userId = token.userId;
                req.data.sessionId = token.sessionId;

                if (fn.isNotPresent(req.data.userId)){
                    throw new UnauthorizedError('Invalid Access Token. Does not contain userId.');
                }
                if (fn.isNotPresent(req.data.sessionId)){
                    throw new UnauthorizedError('Invalid Access Token. Does not contain sessionId.');
                }

                next();
            }
        });
    } else {
        throw new UnauthorizedError('Please log in.');
    }
});

//either provide arrays or null, do not provide empty arrays
const authorisation = (listAllowedRegistrationStates, listAllowedUserTypes, listAllowedUserOrganisations, isNotAllowBlockedUser) => {
    return fn.managedHandler(async (req, res, next) => {
        if (fn.isNotPresent(listAllowedRegistrationStates) || fn.isNotPresent(listAllowedUserTypes) || fn.isNotPresent(listAllowedUserOrganisations) || !fn.isBoolean(isNotAllowBlockedUser)){
            throw new InternalServerError('Please provide the required function parameters, and of the required datatypes.')
        }

        let user = await User.findByPk(req.data.userId);

        if (fn.isNotPresent(user)){
            throw new UnauthorizedError('Invalid identity. Please log in again.')
        }

        if (user.sessionId !== req.data.sessionId) {
            throw new UnauthorizedError('Session expired. Please log in again.')
        }

        if (isNotAllowBlockedUser === true && user.isBlocked === true) {
            throw new ForbiddenError('Your account has been blocked. Please contact support.')
        }

        // todo-upfront customize for your application
        //  CUSTOM FUNCTION BASED ON PROJECT
        //  1. Check if user is at a stage within the allowed registration states
        //  2. Check if user is of allowed organisations (admin, etc)

        req.data.user = user;
        next();
    });
};

//-------------------------------------------END PROJECT CUSTOM MIDDLEWARE--------------------------------------------------------

//===========================================COMMON CONTROLLERS===================================================================

const getApplicationStatus = fn.managedHandler(async (req, res, next) => {
    let listServices = req.query.services;
    if (fn.isNotPresent(listServices)) {
        throw new BadRequestError('The \'services\' query parameter is required.');
    }

    if (!fn.isArrayOfDatatype(listServices, k.DATATYPES.STRING) && !fn.isString(listServices)) {
        throw new BadRequestError('The \'services\' query parameter should be a string or string array.')
    }

    if (fn.isString(listServices)){
        listServices = [listServices];
    }

    let listPromises = [];

    // todo-upfront add the services which should be monitored
    listServices.forEach((service, index) => {
        switch (service) {
            case 'db':
                listPromises.push(fn.measurePromiseDuration(sql.authenticate({
                    benchmark: true,
                })));
                break;
            case 'cache':
                listPromises.push(fn.measurePromiseDuration(cache.ping()));
                break;
            default:
                throw new ForbiddenError('The requested service does not exist: ' + service);
        }
    });

    let results = await Promise.all(listPromises);
    let serviceStatus = {};

    listServices.forEach((service, index) => {
        serviceStatus[service] = results[index] instanceof Error ? -1 : results[index];
    })

    res.send({
        data: serviceStatus,
        cacheStatus: cache.status()
    });

});

//-------------------------------------------END COMMON CONTROLLERS---------------------------------------------------------------
module.exports = {
    beforeRoutes,

    isAuth,
    authorisation,

    multipartFileArray,
    multipartFilesCustom,
    multipartFileSingle,

    requestBodyValidator,
    requestQueryValidator,

    afterRoutes,

    getApplicationStatus
};
