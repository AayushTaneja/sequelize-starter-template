// this file is for storing dynamic constants

// Refer to the following snippet for more information - https://gitlab.com/-/snippets/2180422

const fn = require('/utils/fn');
const k = require('/utils/k');
const cache = require('/utils/cache');
const {InternalServerError, BadRequestError} = require('/utils/error');

class DynamicConstants {
	// todo-upfront change these based on the application
	// If in the database, a key's value exists in the incorrect datatype, it will be set as undefined in the application
	#schema = {
		DYNAMIC_CONSTANT_STRING: k.DATATYPES.STRING,
		DYNAMIC_CONSTANT_BOOLEAN: k.DATATYPES.BOOLEAN,
		DYNAMIC_CONSTANT_NUMBER: k.DATATYPES.NUMBER,
	};

	// This will throw error if accessed any key that is not present or is present with value undefined
	#data = fn.disallowUndefinedProperties({})

	get schema() {
		return this.#schema;
	}

	get data() {
		return (async () => {
			const listDynamicConstants = await cache.get(cache.KEYS.DYNAMIC_CONSTANTS);

			let key, value, keySchema;
			listDynamicConstants.forEach((dynamicConstantObject) => {
				key = dynamicConstantObject.key;
				value = dynamicConstantObject.value;
				keySchema = this.#schema[key];

				if (keySchema !== undefined){
					switch (keySchema) {
						case k.DATATYPES.BOOLEAN:
							switch (value) {
								case 'true':
									this.#data[key] = true;
									break;
								case 'false':
									this.#data[key] = false;
									break;
							}
							break;
						case k.DATATYPES.NUMBER:
							if (!isNaN(Number(value))) {
								this.#data[key] = Number(value);
							}
							break;
						case k.DATATYPES.STRING:
							this.#data[key] = value;
							break;
					}
				}
			})

			return this.#data;
		})();
	}

	validateDynamicConstantTextValue (key, value, isValidationCheck) {
		let ValidationError = isValidationCheck ? BadRequestError : InternalServerError;

		if (!fn.isString(key) || !fn.isString(value)) {
			throw new InternalServerError('The provided key and value should be strings.')
		}

		let dynkSchema = this.#schema;

		if (dynkSchema[key] === undefined) {
			throw new ValidationError('The provided key does not exist in the dynamic constants schema.')
		}

		switch (dynkSchema[key]) {
			case k.DATATYPES.STRING:
				break;
			case k.DATATYPES.NUMBER:
				if (isNaN(Number(value))) {
					throw new ValidationError('The value for the requested key should be a numeric string.');
				}
				break;
			case k.DATATYPES.BOOLEAN:
				if (value !== 'true' && value !== 'false'){
					throw new ValidationError('The value for the requested key should be a numeric string.');
				}
				break;
			default:
				throw new InternalServerError('Invalid datatype has been set for the requested key.');
		}
	}
}

module.exports = fn.disallowUndefinedProperties(new DynamicConstants());
