const fn = require('/utils/fn');
const k = require('/utils/k');
const Ajv = require('ajv');
const ajv = new Ajv();

/*
* This file contains the following validators:
* 1.  string
* 2.  stringFormat (url, ipv4, ipv6, email, phone number, password)
* 3.  integer
* 4.  number
* 5.  boolean
* 6.  dateTime
* 7.  dateOnly
* 8.  timeOnly
* 9.  uuid
* 10. regexp
* 11. object
* 12. array
* */

//ALL FUNCTION PARAMETERS ARE OPTIONAL
// Defaults:
// isRequired = true
// isNotNull = true

//enumArrayOrObject if present, should be a non empty string array or an object
const string = (isRequired, isNotNull, minLength, maxLength, enumArrayOrObject) => {
    let defaultMaxLength = k.FILE_SIZES.KILOBYTE;
    let stringDefinition = {
        type: k.DATATYPES.STRING,
        maxLength: defaultMaxLength,
        errorMessage: {
            type: 'should be a string.',
            maxLength: 'should contain less than or equal to ' + defaultMaxLength + ' characters.'
        },
        transform: ['trim']
    };

    if (fn.isBoolean(isRequired)){
        stringDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isInteger(minLength)){
        stringDefinition.minLength = minLength;
        stringDefinition.errorMessage.minLength = 'should contain more than or equal to ' + minLength + ' characters.';
    }
    else if (fn.isPresent(minLength)){
        throw new Error('Invalid value for parameter minLength');
    }

    if (fn.isInteger(maxLength)){
        stringDefinition.maxLength = maxLength;
        stringDefinition.errorMessage.maxLength = 'should contain less than or equal to ' + maxLength + ' characters.';
    }
    else if (fn.isPresent(maxLength)){
        throw new Error('Invalid value for parameter maxLength');
    }

    if (fn.isPresent(minLength) && fn.isPresent(maxLength)){
        if (minLength > maxLength){
            throw new Error('minLength cannot be greater than maxLength');
        }
    }

    if(fn.isArrayOfDatatype(enumArrayOrObject, k.DATATYPES.STRING) && enumArrayOrObject.length > 0){
        stringDefinition.enum = enumArrayOrObject;
        stringDefinition.errorMessage.enum = 'should be one of the following: ' + enumArrayOrObject;
    }
    else if (fn.isObject(enumArrayOrObject)){
        let listKeys = Object.keys(enumArrayOrObject);
        stringDefinition.enum = listKeys;
        stringDefinition.errorMessage.enum = 'should be one of the following: ' + listKeys;
    }
    else if (fn.isPresent(enumArrayOrObject)){
        throw new Error('Invalid value for parameter enumArray');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            stringDefinition.type = [k.DATATYPES.STRING, 'null'];
            if (stringDefinition.enum){
                stringDefinition.enum.push(null);
            }
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    return stringDefinition;
}

const stringRequiredNotNull = (minLength, maxLength, enumArrayOrObject) => {
    return string(true, true, minLength, maxLength, enumArrayOrObject);
}

const stringRequiredNull = (minLength, maxLength, enumArrayOrObject) => {
    return string(true, false, minLength, maxLength, enumArrayOrObject);
}

const stringOptionalNotNull = (minLength, maxLength, enumArrayOrObject) => {
    return string(false, true, minLength, maxLength, enumArrayOrObject);
}

const stringOptionalNull = (minLength, maxLength, enumArrayOrObject) => {
    return string(false, false, minLength, maxLength, enumArrayOrObject);
}


// k.VAL_FORMAT_URL,
// k.VAL_FORMAT_IPV4,
// k.VAL_FORMAT_IPV6,
// k.VAL_FORMAT_EMAIL,
// k.VAL_FORMAT_PHONE_NUMBER,
// k.VAL_FORMAT_PASSWORD
const stringFormat = (format, isRequired, isNotNull) => {
    if (!Object.values(k.VALIDATION_FORMATS).includes(format)){
        throw new Error('Invalid value for the parameter format')
    }

    let defaultMaxLength = k.FILE_SIZES.KILOBYTE;

    let formatDefinition = {
        type: k.DATATYPES.STRING,
        maxLength: defaultMaxLength,
        format: format,
        transform: ['trim'],
        errorMessage: {
            type: 'should be a string.',
            maxLength: 'should contain less than or equal to ' + defaultMaxLength + ' characters.',
            format: `should be a string of format ${format}`
        }
    };

    if (fn.isBoolean(isRequired)){
        formatDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            formatDefinition.type = [k.DATATYPES.STRING, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    return formatDefinition;
};

const stringFormatRequiredNotNull = (format) => {
    return stringFormat(format, true, true);
}

const stringFormatRequiredNull = (format) => {
    return stringFormat(format, true, false);
}

const stringFormatOptionalNotNull = (format) => {
    return stringFormat(format, false, true);
}

const stringFormatOptionalNull = (format) => {
    return stringFormat(format, false, false);
}


//enumArray if present, should be a non empty number array
const integer = (isRequired, isNotNull, minimum, maximum, enumArray, multipleOf) => {
    let defaultMaximum = 10 ** 12;      // One trillion ~ 10^12
    let integerDefinition = {
        type: k.DATATYPES.INTEGER,
        maximum: defaultMaximum,
        errorMessage: {
            type: 'should be an integer.',
            maximum: 'should be less than or equal to  1 trillion ~ 10^12.'
        }
    };

    if (fn.isBoolean(isRequired)){
        integerDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isInteger(minimum)){
        integerDefinition.minimum = minimum;
        integerDefinition.errorMessage.minimum = 'should be more than or equal to ' + minimum;
    }
    else if (fn.isPresent(minimum)){
        throw new Error('Invalid value for parameter minimum');
    }

    if (fn.isInteger(maximum)){
        integerDefinition.maximum = maximum;
        integerDefinition.errorMessage.maximum = 'should be less than or equal to ' + maximum;
    }
    else if (fn.isPresent(maximum)){
        throw new Error('Invalid value for parameter maximum');
    }

    if (fn.isPresent(minimum) && fn.isPresent(maximum)){
        if (minimum > maximum){
            throw new Error('minimum cannot be greater than maximum');
        }
    }

    if(fn.isArrayOfDatatype(enumArray, k.DATATYPES.NUMBER) && enumArray.length > 0){
        integerDefinition.enum = enumArray;
        integerDefinition.errorMessage.enum = 'should be one of the following: ' + enumArray;
    }
    else if (fn.isPresent(enumArray)){
        throw new Error('Invalid value for parameter enumArray');
    }

    if (fn.isInteger(multipleOf)){
        integerDefinition.multipleOf = multipleOf;
        integerDefinition.errorMessage.multipleOf = 'should be a multiple of ' + multipleOf;
    }
    else if (fn.isPresent(multipleOf)){
        throw new Error('Invalid value for parameter multipleOf');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            integerDefinition.type = ['integer', 'null'];
            if(integerDefinition.enum){
                integerDefinition.enum.push(null);
            }
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    return integerDefinition;
};

const integerRequiredNotNull = (minimum, maximum, enumArray, multipleOf) => {
    return integer(true, true, minimum, maximum, enumArray, multipleOf);
}

const integerRequiredNull = (minimum, maximum, enumArray, multipleOf) => {
    return integer(true, false, minimum, maximum, enumArray, multipleOf);
}

const integerOptionalNotNull = (minimum, maximum, enumArray, multipleOf) => {
    return integer(false, true, minimum, maximum, enumArray, multipleOf);
}

const integerOptionalNull = (minimum, maximum, enumArray, multipleOf) => {
    return integer(false, false, minimum, maximum, enumArray, multipleOf);
}


//enumArray if present, should be a non empty number array
const number = (isRequired, isNotNull, minimum, maximum, enumArray) => {
    let defaultMaximum = 10 ** 12;      // One trillion ~ 10^12
    let numberDefinition = {
        type: k.DATATYPES.NUMBER,
        maximum: defaultMaximum,
        errorMessage: {
            type: 'should be a number.',
            maximum: 'should be less than or equal to 1 trillion ~ 10^12'
        }
    };

    if (fn.isBoolean(isRequired)){
        numberDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isNumber(minimum)){
        numberDefinition.minimum = minimum;
        numberDefinition.errorMessage.minimum = 'should be more than or equal to ' + minimum;
    }
    else if (fn.isPresent(minimum)){
        throw new Error('Invalid value for parameter minimum');
    }

    if (fn.isNumber(maximum)){
        numberDefinition.maximum = maximum
        numberDefinition.errorMessage.maximum = 'should be less than or equal to ' + maximum;
    }
    else if (fn.isPresent(maximum)){
        throw new Error('Invalid value for parameter maximum');
    }

    if (fn.isPresent(minimum) && fn.isPresent(maximum)){
        if (minimum > maximum){
            throw new Error('minimum cannot be greater than maximum');
        }
    }

    if(fn.isArrayOfDatatype(enumArray, k.DATATYPES.NUMBER) && enumArray.length > 0){
        numberDefinition.enum = enumArray
        numberDefinition.errorMessage.enum = 'should be one of the following: ' + enumArray;
    }
    else if (fn.isPresent(enumArray)){
        throw new Error('Invalid value for parameter enumArray');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            numberDefinition.type = [k.DATATYPES.NUMBER, 'null'];
            if (numberDefinition.enum){
                numberDefinition.enum.push(null);
            }
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    // if (fn.isNumber(multipleOf)){
    //     numberDefinition.multipleOf = multipleOf
    // }
    // else if (fn.isNotNullNotUndefined(multipleOf)){
    //     throw new Error('Invalid value for parameter multipleOf');
    // }

    return numberDefinition;
}

const numberRequiredNotNull = (minimum, maximum, enumArray) => {
    return number(true, true, minimum, maximum, enumArray);
}

const numberRequiredNull = (minimum, maximum, enumArray) => {
    return number(true, false, minimum, maximum, enumArray);
}

const numberOptionalNotNull = (minimum, maximum, enumArray) => {
    return number(false, true, minimum, maximum, enumArray);
}

const numberOptionalNull = (minimum, maximum, enumArray) => {
    return number(false, false, minimum, maximum, enumArray);
}



const boolean = (isRequired, isNotNull) => {
    let booleanDefinition = {
        type: k.DATATYPES.BOOLEAN
    };

    if (fn.isBoolean(isRequired)){
        booleanDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            booleanDefinition.type = [k.DATATYPES.BOOLEAN, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    return booleanDefinition;
}

const booleanRequiredNotNull = () => {
    return boolean(true, true);
}

const booleanRequiredNull = () => {
    return boolean(true, false);
}

const booleanOptionalNotNull = () => {
    return boolean(false, true);
}

const booleanOptionalNull = () => {
    return boolean(false, false);
}



const dateOnly = (isRequired, isNotNull, dateOnlyMinimum, dateOnlyMaximum) => {
    let formatDefinition = {
        format: k.VALIDATION_FORMATS.DATE_ONLY,
        type: k.DATATYPES.STRING,
        errorMessage: {
            type: 'should be a date string.',
            format: 'should be a date of format YYYY-MM-DD'
        }
    };

    if (fn.isBoolean(isRequired)){
        formatDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }


    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            formatDefinition.type = [k.DATATYPES.STRING, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    let validateDate = ajv.compile({properties: {date: {type: k.DATATYPES.STRING, format: k.VALIDATION_FORMATS.DATE_ONLY}}, additionalProperties: false});
    if (validateDate({date: dateOnlyMinimum})){
        formatDefinition.formatMinimum = dateOnlyMinimum;
        formatDefinition.errorMessage.formatMinimum = 'should be more than or equal to ' + dateOnlyMinimum;
    }
    else if (fn.isPresent(dateOnlyMinimum)){
        throw new Error('Invalid value for parameter formatMinimum. Should match YYYY-MM-DD');
    }

    if (validateDate({date: dateOnlyMaximum})){
        formatDefinition.formatMaximum = dateOnlyMaximum;
        formatDefinition.errorMessage.formatMaximum = 'should be less than or equal to ' + dateOnlyMaximum;
    }
    else if (fn.isPresent(dateOnlyMaximum)){
        throw new Error('Invalid value for parameter formatMaximum. Should match YYYY-MM-DD');
    }

    return formatDefinition;
};

const dateOnlyRequiredNotNull = (dateOnlyMinimum, dateOnlyMaximum) => {
    return dateOnly(true, true, dateOnlyMinimum, dateOnlyMaximum);
}

const dateOnlyRequiredNull = (dateOnlyMinimum, dateOnlyMaximum) => {
    return dateOnly(true, false, dateOnlyMinimum, dateOnlyMaximum);
}

const dateOnlyOptionalNotNull = (dateOnlyMinimum, dateOnlyMaximum) => {
    return dateOnly(false, true, dateOnlyMinimum, dateOnlyMaximum);
}

const dateOnlyOptionalNull = (dateOnlyMinimum, dateOnlyMaximum) => {
    return dateOnly(false, false, dateOnlyMinimum, dateOnlyMaximum);
}



const timeOnly = (isRequired, isNotNull, timeOnlyMinimum, timeOnlyMaximum) => {
    let formatDefinition = {
        type: k.DATATYPES.STRING,
        format: k.VALIDATION_FORMATS.TIME_ONLY,
        errorMessage: {
            type: 'should be a time string.',
            format: 'should be a 24 hr time of format HH:mm:ss',
        }
    };

    if (fn.isBoolean(isRequired)){
        formatDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            formatDefinition.type = [k.DATATYPES.STRING, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    let validateTime = ajv.compile({properties: {time: {type: k.DATATYPES.STRING, format: k.VALIDATION_FORMATS.TIME_ONLY}}, additionalProperties: false});
    if (validateTime({time: timeOnlyMinimum})){
        formatDefinition.formatMinimum = timeOnlyMinimum;
        formatDefinition.errorMessage.formatMinimum = 'should be more than or equal to ' + timeOnlyMinimum;
    }
    else if (fn.isPresent(timeOnlyMinimum)){
        throw new Error('Invalid value for parameter formatMinimum. Should match HH:mm:ss');
    }

    if (validateTime({time: timeOnlyMaximum})){
        formatDefinition.formatMaximum = timeOnlyMaximum;
        formatDefinition.errorMessage.formatMaximum = 'should be less than or equal to ' + timeOnlyMaximum;
    }
    else if (fn.isPresent(timeOnlyMaximum)){
        throw new Error('Invalid value for parameter formatMaximum. Should match HH:mm:ss');
    }

    return formatDefinition;
};

const timeOnlyRequiredNotNull = (timeOnlyMinimum, timeOnlyMaximum) => {
    return timeOnly(true, true, timeOnlyMinimum, timeOnlyMaximum);
}

const timeOnlyRequiredNull = (timeOnlyMinimum, timeOnlyMaximum) => {
    return timeOnly(true, false, timeOnlyMinimum, timeOnlyMaximum);
}

const timeOnlyOptionalNotNull = (timeOnlyMinimum, timeOnlyMaximum) => {
    return timeOnly(false, true, timeOnlyMinimum, timeOnlyMaximum);
}

const timeOnlyOptionalNull = (timeOnlyMinimum, timeOnlyMaximum) => {
    return timeOnly(false, false, timeOnlyMinimum, timeOnlyMaximum);
}



const dateTime = (isRequired, isNotNull, dateTimeMinimum, dateTimeMaximum) => {
    let formatDefinition = {
        type: k.DATATYPES.STRING,
        format: k.VALIDATION_FORMATS.DATE_TIME,
        errorMessage:{
            type: 'should be a date-time string.',
            format: 'should match RFC3339 yyyy-MM-dd\'T\'HH:mm:ss.SSSZZZZZ date format.',
        }
    };

    if (fn.isBoolean(isRequired)){
        formatDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }


    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            formatDefinition.type = [k.DATATYPES.STRING, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    let validateDateTime = ajv.compile({properties: {dateTime: {type: k.DATATYPES.STRING, format: k.VALIDATION_FORMATS.DATE_TIME}}, additionalProperties: false});
    if (validateDateTime({dateTime: dateTimeMinimum})){
        formatDefinition.formatMinimum = dateTimeMinimum;
        formatDefinition.errorMessage.formatMinimum = 'should be more than or equal to ' + dateTimeMinimum;
    }
    else if (fn.isPresent(dateTimeMinimum)){
        throw new Error('Invalid value for parameter formatMinimum. Should match yyyy-MM-dd\'T\'HH:mm:ss.SSSZZZZZ');
    }

    if (validateDateTime({dateTime: dateTimeMaximum})){
        formatDefinition.formatMaximum = dateTimeMaximum;
        formatDefinition.errorMessage.formatMaximum = 'should be less than or equal to ' + dateTimeMaximum;
    }
    else if (fn.isPresent(dateTimeMaximum)){
        throw new Error('Invalid value for parameter formatMaximum. Should match yyyy-MM-dd\'T\'HH:mm:ss.SSSZZZZZ');
    }

    return formatDefinition;
};

const dateTimeRequiredNotNull = (dateTimeMinimum, dateTimeMaximum) => {
    return dateTime(true, true, dateTimeMinimum, dateTimeMaximum);
}

const dateTimeRequiredNull = (dateTimeMinimum, dateTimeMaximum) => {
    return dateTime(true, false, dateTimeMinimum, dateTimeMaximum);
}

const dateTimeOptionalNotNull = (dateTimeMinimum, dateTimeMaximum) => {
    return dateTime(false, true, dateTimeMinimum, dateTimeMaximum);
}

const dateTimeOptionalNull = (dateTimeMinimum, dateTimeMaximum) => {
    return dateTime(false, false, dateTimeMinimum, dateTimeMaximum);
}



const uuid = (isRequired, isNotNull) => {
    let formatDefinition = {
        type: k.DATATYPES.STRING,
        format: k.VALIDATION_FORMATS.UUID,
        errorMessage:{
            type: 'should be a UUID format string.',
            format: 'should be a UUID format string.',
        }
    };

    if (fn.isBoolean(isRequired)){
        formatDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            formatDefinition.type = [k.DATATYPES.STRING, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    return formatDefinition;
};

const uuidRequiredNotNull = () => {
    return uuid(true, true);
}

const uuidRequiredNull = () => {
    return uuid(true, false);
}

const uuidOptionalNotNull = () => {
    return uuid(false, true);
}

const uuidOptionalNull = () => {
    return uuid(false, false);
}


const object = (schema, isRequired, isNotNull, dependencies) => {
    if (!fn.isObject(schema) || Object.keys(schema).length === 0){
        throw new Error('Invalid schema.')
    }

    let objectDefinition = {
        type: k.DATATYPES.OBJECT,
        required: [],
        properties: schema,
        additionalProperties: false,
    };

    if (fn.isPresent(dependencies)) {
        objectDefinition.dependencies = dependencies;
    }

    if (fn.isBoolean(isRequired)){
        objectDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }


    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            objectDefinition.type = [k.DATATYPES.OBJECT, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }

    Object.keys(schema).forEach((key, index) => {
        if(!schema[key].isOptional){
            objectDefinition.required.push(key);
        }
    });

    objectDefinition.errorMessage = {
        type: 'should be an object.',
        required: 'must have all of the following fields: ' + objectDefinition.required,
        additionalProperties: 'should not have any properties other than: ' + Object.keys(schema),
    }

    return objectDefinition;
}

/*
* dependencies: {
*   'foo': ['bar', 'cat']       // If field 'foo' is present, then fields 'bar' and 'cat' should also be present
* }
* */

const objectRequiredNotNull = (schema, dependencies) => {
    return object(schema, true, true, dependencies);
}

const objectRequiredNull = (schema, dependencies) => {
    return object(schema, true, false, dependencies);
}

const objectOptionalNotNull = (schema, dependencies) => {
    return object(schema, false, true, dependencies);
}

const objectOptionalNull = (schema, dependencies) => {
    return object(schema, false, false, dependencies);
}


const array = (itemObject, isRequired, isNotNull, minItems, maxItems, isUniqueItems, listUniqueItemProperties) => {
    if (fn.isNotPresent(itemObject)){
        throw new Error('Item Object is a required function parameter.');
    }

    // if (!fn.isNotNullNotUndefined(itemObject.type) || !fn.isNotNullNotUndefined(itemObject.required) || !fn.isNotNullNotUndefined(itemObject.properties) || !fn.isNotNullNotUndefined(itemObject.additionalProperties)){
    //     throw new Error('Invalid Item Object.');
    // }

    if (fn.isPresent(itemObject.isOptional) && itemObject.isOptional !== false){
        throw new Error('Item Object cannot be optional.');
    }

    let arrayDefinition = {
        type: k.DATATYPES.ARRAY,
        items: itemObject,
        additionalItems: false,
        errorMessage: {
            type: 'should be an array.',
            additionalItems: 'should not contain any additional items.'
        }
    };


    if (fn.isBoolean(isRequired)){
        arrayDefinition.isOptional = !isRequired;
    }
    else if (fn.isPresent(isRequired)){
        throw new Error('Invalid value for parameter isRequired');
    }

    if (fn.isBoolean(isNotNull)){
        if(!isNotNull){
            arrayDefinition.type = [k.DATATYPES.ARRAY, 'null'];
        }
    }
    else if (fn.isPresent(isNotNull)){
        throw new Error('Invalid value for parameter isNotNull');
    }


    if (fn.isInteger(minItems)){
        arrayDefinition.minItems = minItems;
        arrayDefinition.errorMessage.minItems = 'should not have fewer than ' + minItems + ' items.';
    }
    else if (fn.isPresent(minItems)){
        throw new Error('Invalid value for minItems.');
    }

    if (fn.isInteger(maxItems)){
        arrayDefinition.maxItems = maxItems;
        arrayDefinition.errorMessage.maxItems = 'should not have more than ' + maxItems + ' items.';
    }
    else if (fn.isPresent(maxItems)){
        throw new Error('Invalid value for maxItems.');
    }

    if (fn.isBoolean(isUniqueItems)){
        arrayDefinition.uniqueItems = isUniqueItems;
    }
    else if (fn.isPresent(isUniqueItems)){
        throw new Error('Invalid value for isUniqueItems.');
    }
    else {
        if (fn.isArrayOfDatatype(listUniqueItemProperties, k.DATATYPES.STRING) && listUniqueItemProperties.length > 0){
            if (!itemObject.properties){
                throw new Error('itemObject is a primitive and hence cannot support listUniqueItemProperties.');
            }
            let objectProperties = Object.keys(itemObject.properties);
            listUniqueItemProperties.forEach((property, index) => {
                if (!objectProperties.includes(property)){
                    throw new Error('Remove property from listUniqueItemProperties: ' + property);
                }
            });
            arrayDefinition.uniqueItemProperties = listUniqueItemProperties;
        }
        else if (fn.isPresent(listUniqueItemProperties)){
            throw new Error('Invalid value for listUniqueItemProperties.');
        }
    }

    return arrayDefinition;
};

const arrayRequiredNotNull = (itemObject, minItems, maxItems, isUniqueItems, listUniqueItemProperties) => {
    return array(itemObject, true, true, minItems, maxItems, isUniqueItems, listUniqueItemProperties);
}

const arrayRequiredNull = (itemObject, minItems, maxItems, isUniqueItems, listUniqueItemProperties) => {
    return array(itemObject, true, false, minItems, maxItems, isUniqueItems, listUniqueItemProperties);
}

const arrayOptionalNotNull = (itemObject, minItems, maxItems, isUniqueItems, listUniqueItemProperties) => {
    return array(itemObject, false, true, minItems, maxItems, isUniqueItems, listUniqueItemProperties);
}

const arrayOptionalNull = (itemObject, minItems, maxItems, isUniqueItems, listUniqueItemProperties) => {
    return array(itemObject, false, false, minItems, maxItems, isUniqueItems, listUniqueItemProperties);
}



module.exports = {
    stringRequiredNotNull,
    stringRequiredNull,
    stringOptionalNotNull,
    stringOptionalNull,

    stringFormatRequiredNotNull,
    stringFormatRequiredNull,
    stringFormatOptionalNotNull,
    stringFormatOptionalNull,

    integerRequiredNotNull,
    integerRequiredNull,
    integerOptionalNotNull,
    integerOptionalNull,

    numberRequiredNotNull,
    numberRequiredNull,
    numberOptionalNotNull,
    numberOptionalNull,

    booleanRequiredNotNull,
    booleanRequiredNull,
    booleanOptionalNotNull,
    booleanOptionalNull,

    dateOnlyRequiredNotNull,
    dateOnlyRequiredNull,
    dateOnlyOptionalNotNull,
    dateOnlyOptionalNull,

    timeOnlyRequiredNotNull,
    timeOnlyRequiredNull,
    timeOnlyOptionalNotNull,
    timeOnlyOptionalNull,

    dateTimeRequiredNotNull,
    dateTimeRequiredNull,
    dateTimeOptionalNotNull,
    dateTimeOptionalNull,

    uuidRequiredNotNull,
    uuidRequiredNull,
    uuidOptionalNotNull,
    uuidOptionalNull,

    objectRequiredNotNull,
    objectRequiredNull,
    objectOptionalNotNull,
    objectOptionalNull,

    arrayRequiredNotNull,
    arrayRequiredNull,
    arrayOptionalNotNull,
    arrayOptionalNull,
}