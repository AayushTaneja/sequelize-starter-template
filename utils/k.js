// this file is for storing the static constants and categories

const disallowUndefinedProperties = (obj) => {
    const handler = {
        get(target, property) {
            if (property in target) {
                return target[property];
            }

            throw new Error(`Property '${property}' is not defined`);
        }
    };

    return new Proxy(obj, handler);
}

// This will throw an error if any constant key that does not exist is accessed in the application
let k = disallowUndefinedProperties({});

// Regular Expressions
k.REGEXPS = disallowUndefinedProperties({
    PHONE_NUMBER: /^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,6}$/,
    PASSWORD: /^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$/,   // Minimum eight characters, at least one letter, one number and one special character
});

// todo-upfront change this based on the Database used by the application
// SQL Database Dialect
k.SQL_DIALECT = 'postgres';


//Database Error Codes
k.DB_ERROR_CODES = disallowUndefinedProperties({
    UNIQUE_CONSTRAINT_VIOLATED: '23505',
    FOREIGN_KEY_VIOLATED: '23503',
    CHECK_CONSTRAINT_VIOLATED: '23514',
});


//Node environments
k.NODE_ENVS = disallowUndefinedProperties({
    LOCAL: 'local',
    TEST: 'test',
    DEVELOPMENT: 'development',
    STAGING: 'staging',
    PRODUCTION: 'production',
});


// Common Datatypes
k.DATATYPES = disallowUndefinedProperties({
    STRING: 'string',
    INTEGER: 'integer',
    NUMBER: 'number',
    BOOLEAN: 'boolean',
    OBJECT: 'object',
    ARRAY: 'array',
});


//Validation data types and formats
k.VALIDATION_FORMATS = disallowUndefinedProperties({
    DATE_ONLY: 'date',
    TIME_ONLY: 'time',
    DATE_TIME: 'date-time',
    UUID: 'uuid',

    // Validation Formats to be used with vx.stringFormat validators
    EMAIL: 'email',
    URL: 'uri',
    HOSTNAME: 'hostname',
    IPV4: 'ipv4',
    IPV6: 'ipv6',

    // Custom Formats (Check the Regex Constants in this file to find the custom format regular expressions)
    PHONE_NUMBER: 'phoneNumber',
    PASSWORD: 'password'    // Minimum eight characters, at least one letter, one number and one special character
});


//File MimeType Formats
k.FILE_MIMETYPES = disallowUndefinedProperties({
    IMAGE_PNG: 'image/png',
    IMAGE_JPEG: 'image/jpeg',
    APPLICATION_PDF: 'application/pdf',
});


// Durations
k.DURATIONS_IN_SECONDS = {
    SECOND: 1,
    MINUTE: 60,
    HOUR: 3600,
    DAY: 86400
};

k.DURATIONS_IN_MILLISECONDS = {
    SECOND: k.DURATIONS_IN_SECONDS.SECOND * 1000,
    MINUTE: k.DURATIONS_IN_SECONDS.MINUTE * 1000,
    HOUR: k.DURATIONS_IN_SECONDS.HOUR * 1000,
    DAY: k.DURATIONS_IN_SECONDS.DAY * 1000
}


//File Size Values
k.FILE_SIZES = disallowUndefinedProperties({
    KILOBYTE: 1024,
});
k.FILE_SIZES.MEGABYTE = 1024 * k.FILE_SIZES.KILOBYTE;


//JWT Encoding Mechanism
k.JWT_OPTIONS = {
    algorithm: 'RS256',
    expiresIn: '30d'
};


// Other Literals
k.POSTGRES_DATATYPE_GEOMETORY_POINT = 'Point';


// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// todo-upfront customize all the following constants based on your application

// Request Max Size
k.REQUEST_FILE_SIZE_MAX = 10 * k.FILE_SIZES.MEGABYTE;
k.REQUEST_DATA_SIZE_MAX = 2 * k.FILE_SIZES.KILOBYTE;
k.REQUEST_TOTAL_SIZE_MAX = k.REQUEST_FILE_SIZE_MAX + k.REQUEST_DATA_SIZE_MAX;

// Cache Keys
k.CACHE_KEYS = {
    DYNAMIC_CONSTANTS: 'DYNAMIC_CONSTANTS',
    // ... add other category cache keys
}


// Pagination Limit Values

k.PAGE_LIMIT_DEFAULT = 10;


// Named Categories

k.USER_GENDER_TYPES = {
    MALE: 'MALE',
    FEMALE: 'FEMALE',
    OTHERS: 'OTHERS',
};

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Object.freeze(k);                   // This only freezes the root level properties of the object and not the properties of its nested objects.
module.exports = k;
