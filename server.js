require('module-alias/register');		// In VSCode, to support absolute path autocomplete - install extension "Path Autocomplete"

const config = require ('/utils/config');
const k = require('/utils/k');
const log = require('/utils/log');
const app = require('/app');
const port = config.PORT || 8080;
const cluster = require('cluster');

if (config.NODE_ENV === k.NODE_ENVS.LOCAL) {
	app.listen(port, () => {
		log.info(`Starting server at port: ${port}`);
	})
}
else {
	let numCPUs = require('os').cpus().length;
	if (cluster.isMaster) {
		for (let i = 0; i < numCPUs; i++) {
			cluster.fork();
		}
		cluster.on('exit', (worker, code, signal) => {
			console.log(`Worker Died => processId: ${worker.process.pid}`);
			cluster.fork();
		});
	} else {
		app.listen(port, () => {
			log.info(`Starting server at port: ${port} => processId: ${process.pid}`);
		});
	}
}



