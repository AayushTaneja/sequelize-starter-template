const Sequelize = require('sequelize');
const sql = require('/database/sql');
const User = require('/models/user');

let model =  sql.define(
    'model',                           //table name
    {                                   //model description



        genericField: {                     //field name | camel case in node -> snake_case in database
            type: Sequelize.STRING,             //type
            allowNull: false,                   //allowNull
            defaultValue: "Aayush",             //defaultValue



            validate: {                         // validations run on CREATE, UPDATE, SAVE and VALIDATE operations

                notNull: {                      //Custom error message for allowNull = false;
                    msg: 'Please enter your name'
                },

                is: ["^[a-z]+$",'i'],     // will only allow letters
                // is: /^[a-z]+$/i,          // same as the previous example using real RegExp
                not: ["[a-z]",'i'],       // will not allow letters
                isEmail: true,            // checks for email format (foo@bar.com)
                isUrl: true,              // checks for url format (http://foo.com)
                isIP: true,               // checks for IPv4 (129.89.23.1) or IPv6 format
                isIPv4: true,             // checks for IPv4 (129.89.23.1)
                isIPv6: true,             // checks for IPv6 format
                isAlpha: true,            // will only allow letters
                isAlphanumeric: true,     // will only allow alphanumeric characters, so "_abc" will fail
                isNumeric: true,          // will only allow numbers
                // isInt: true,              // checks for valid integers
                isFloat: true,            // checks for valid floating point numbers
                isDecimal: true,          // checks for any numbers
                isLowercase: true,        // checks for lowercase
                isUppercase: true,        // checks for uppercase
                // notNull: true,            // won't allow null
                isNull: true,             // only allows null
                notEmpty: true,           // don't allow empty strings
                equals: 'specific value', // only allow a specific value
                contains: 'foo',          // force specific substrings
                notIn: [['foo', 'bar']],  // check the value is not one of these
                // isIn: [['foo', 'bar']],   // check the value is one of these
                notContains: 'bar',       // don't allow specific substrings
                len: [2,10],              // only allow values with length between 2 and 10
                isUUID: 4,                // only allow uuids
                isDate: true,             // only allow date strings
                isAfter: "2011-11-05",    // only allow date strings after a specific date
                isBefore: "2011-11-05",   // only allow date strings before a specific date
                max: 23,                  // only allow values <= 23
                min: 23,                  // only allow values >= 23
                isCreditCard: true,       // check for valid credit card numbers


                isEven(value) {                 // Examples of custom validators (RUN EVEN WHEN allowNull = true)
                    if (parseInt(value) % 2 !== 0) {
                        throw new Error('Only even values are allowed!');
                    }
                },
                isGreaterThanOtherField(value) {
                    if (parseInt(value) <= parseInt(this.otherField)) {
                        throw new Error('Bar must be greater than otherField.');
                    }
                },

                isInt: {
                    msg: "Must be an integer number of pennies"
                },
                isIn: {
                    args: [['en', 'zh']],
                    msg: "Must be English or Chinese"
                }

            },



            //Foreign Key
            references: {
                // This is a reference to another model
                model: User,

                // This is the column name of the referenced model
                key: 'id',

                // With PostgreSQL, it is optionally possible to declare when to check the foreign key constraint, passing the Deferrable type.
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
                // Options:
                // - `Deferrable.INITIALLY_IMMEDIATE` - Immediately check the foreign key constraints
                // - `Deferrable.INITIALLY_DEFERRED` - Defer all foreign key constraint check to the end of a transaction
                // - `Deferrable.NOT` - Don't defer the checks at all (default) - This won't allow you to dynamically change the rule in a transaction
            }
        },



        id:{
            type:Sequelize.UUIDV4,
            allowNull:false,
        },
        createdAt:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW
        },
        updatedAt:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW
        },
        deletedAt:{
            type:Sequelize.DATE
        },





        uniqueField:{                              //unique index for single field
            type:Sequelize.STRING,
            unique:true
        },




        uniqueOne: {                                //composite unique index
            type: Sequelize.STRING,
            unique: 'compositeIndex'
        },
        uniqueTwo: {
            type: Sequelize.INTEGER,
            unique: 'compositeIndex'
        },
    },
    {                                      //Options


        getterMethods: {                        //Getter methods
            get fullName() {
                return this.getDataValue('fullName');
            },
            get height(){
                return this.getDataValue('height');     //Can be used to extract values of attributes within functions
            }
        },
        setterMethods: {                        //Setter Methods
            set fullName(fullName) {
                //
            },
            set height(height){
                //
            }
        },
        classMethods: {				//class level static methods
            associate: function(models) {
                // Tod.belongsTo(models.User);
            }
        },
        instanceMethods: {			//instance methods
            sayTitle: function() {
                console.log(this.title)
            }
        },



        validate: {                             //Model wide validation
            bothCoordsOrNone() {
                if ((this.latitude === null) !== (this.longitude === null)) {
                    throw new Error('Require either both latitude and longitude or neither')
                }
            }
        },


    }
);
