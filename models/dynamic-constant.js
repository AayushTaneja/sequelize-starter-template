const Sequelize = require('sequelize');
const sql = require('/database/sql');

const DynamicConstant = sql.define(
	'DynamicConstant',
	{
		key: {
			type: Sequelize.TEXT,
			primaryKey: true
		},
		createdAt: {
			type: Sequelize.DATE,
			allowNull: false
		},
		updatedAt: {
			type: Sequelize.DATE,
			allowNull: false
		},
		value: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		description: {
			type: Sequelize.STRING,
			allowNull: true,
		}
	},
	{}
);

module.exports = DynamicConstant;