const Sequelize = require('sequelize');
const sql = require('/database/sql');
const fn = require('/utils/fn');

const User = sql.define(
    'User',
    {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true
        },
        createdAt: {
            type: Sequelize.DATE,
            allowNull: false
        },
        updatedAt: {
            type: Sequelize.DATE,
            allowNull: false
        },
        sessionId: {
            type: Sequelize.UUID,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4,
        },
        isBlocked: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
        //todo-upfront customize based on your application
    },
    {}
);


//todo-upfront customize based on your application
let catchCreateHandler = (e) => {
    fn.catchDatabaseErrors(e,
        [
            fn.uniqueConstraint(['email'], 'This account already exists. Please login instead.')
        ],
        [
            fn.foreignKeyConstraint('locationCode',  'Please select a valid country and state.')
        ],
        []
    )
}

// todo-upfront customize based on your application
let catchUpdateHandler = (e) => {
    fn.catchDatabaseErrors(e,
        [
            fn.uniqueConstraint(['email'], 'This account already exists. Please login instead.')
        ],
        [
            fn.foreignKeyConstraint('locationCode',  'Please select a valid country and state.')
        ],
        []
    )
}

fn.setModelServices(User, catchCreateHandler, catchUpdateHandler);

module.exports = User ;
