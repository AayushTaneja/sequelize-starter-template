const express = require('express');
const mw = require('/utils/mw');
const setSqlAssociations = require('/database/associations');
const routerV1 = require('/v1/router');
const fn = require('/utils/fn');
let app;

// todo test the dynamic constants route
// todo Create a walkthrough - Sample Task Management App and cover as much features as possible, even testing and mocking

process.on('uncaughtException', async error => {
    error.isUncaughtException = true;
    await fn.sanitizeAndReportError('UNCAUGHT EXCEPTION => ' + error);
});

process.on('unhandledRejection', async (error, promise) => {
    error.isUnhandledRejection = true;
    await fn.sanitizeAndReportError('UNHANDLED REJECTION => ' + error);
});

app = express();

// todo-upfront create entity based files using the following program -- https://gitlab.com/apheliainnovations/nodejs-entity-files-generator
mw.beforeRoutes(app);
app.use('/status', mw.getApplicationStatus)
app.use('/v1', routerV1);
mw.afterRoutes(app);

setSqlAssociations();

module.exports = app;
