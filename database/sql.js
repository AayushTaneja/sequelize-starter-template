const cls = require('cls-hooked');
const namespace = cls.createNamespace('transaction');
const Sequelize = require('sequelize');
const config = require('/utils/config');
const k = require('/utils/k');

Sequelize.useCLS(namespace);        //For automatic usage of default transaction in all database operations

let isSqlLoggingEnabled = config.NODE_ENV === k.NODE_ENVS.LOCAL ? console.log : false;

const sql = new Sequelize(
    config.SQL_DB,     //database
    config.SQL_USER,     //username
    config.SQL_PW,     //password
    {
        dialect: k.SQL_DIALECT,
        host: config.SQL_HOST,
        logging: isSqlLoggingEnabled,
        define: {
            freezeTableName: true,      //associate with table name as it is w/o pluralising
            paranoid: false,            //not have soft deletes using deletedAt
            timestamps: true,           //have createdAt and updatedAt
            underscored:false,          //no snake_case field names
        },
        dialectOptions: {
            // connectionTimeoutMillis: 10000,
            statement_timeout: 5000,
            idle_in_transaction_session_timeout: 10000
        },
        pool: {
            max: 50,
            min: 0,
            idle: k.DURATIONS_IN_MILLISECONDS.MINUTE,
            evict: k.DURATIONS_IN_MILLISECONDS.SECOND,
            acquire: 10 * k.DURATIONS_IN_MILLISECONDS.SECOND,
            autostart: true
        }
    }
);

sql.startNewTransaction = (callbackFunction) => {
    return sql.transaction(
        {
            isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
        },
        callbackFunction)
};

module.exports = sql;



