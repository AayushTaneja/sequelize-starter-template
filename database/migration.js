require('module-alias/register');		// In VSCode, to support absolute path autocomplete - install extension "Path Autocomplete"
const config = require('/utils/config');
const k = require('/utils/k');

module.exports = {
    "local": {
        username: config.SQL_USER,
        password: config.SQL_PW,
        database: config.SQL_DB,
        host: config.SQL_HOST,
        dialect: k.SQL_DIALECT
    },
    "test": {
        username: config.SQL_USER,
        password: config.SQL_PW,
        database: config.SQL_DB,
        host: config.SQL_HOST,
        dialect: k.SQL_DIALECT
    },
    "development": {
        username: config.SQL_USER,
        password: config.SQL_PW,
        database: config.SQL_DB,
        host: config.SQL_HOST,
        dialect: k.SQL_DIALECT
    },
    "staging": {
        username: config.SQL_USER,
        password: config.SQL_PW,
        database: config.SQL_DB,
        host: config.SQL_HOST,
        dialect: k.SQL_DIALECT
    },
    "production": {
        username: config.SQL_USER,
        password: config.SQL_PW,
        database: config.SQL_DB,
        host: config.SQL_HOST,
        dialect: k.SQL_DIALECT
    },
};